$( document ).ready(function(){
	
    var entry='';
    $.ajax({
        url: '/getClassDetails',
        type: 'GET',
        success: function(data){
        	jQuery.each(data, function(index, item) {
        		
               entry =  '<a href="school-upload-content-subject?classId=' + item.classId  + '">'
            	   + '<div class="row rounded chapter text-dark border">'
            	   +'<div class="col-md-2 mb-2"><strong>' + item.className + '</strong></div>'
            	   +' <div class="col-md-5 mb-2">' + item.subjectCount + ' Subjects with ' + item.chapterCount  + ' Chapters</div>'
            	   + '<div class="col-md-3 mb-2"> ' 
            	   +' <div class="progress">'
            	   + '<div class="progress-bar" role="progressbar" aria-valuenow="' + 83 + '" aria-valuemin="0" aria-valuemax=" ' + 83 + '"></div>'
            	   + ' </div> </div>'                       
            	   + ' <div class="col-md-2 mb-2">'+ 67 +'/'+ 84 + '<i class="far fa-arrow-alt-circle-right mt-1 float-right" data-toggle="tooltip" data-placement="top" title="View Details"></i></div>'
            	   + '</div> </a>'

               $('#chapterlist').append(entry)
                             		
           });
        	
        	
        }
    });
	
	
	
	
	
	$.ajax({
        type: "GET",
        url: "/getAllClasses",
        success: function(result)
        {
        	console.log(JSON.stringify(result));
        	classhelpers.buildDropdown(
                jQuery.parseJSON(JSON.stringify(result)),
                $('#secClass'),
                'Select an option'
            );
        }
    });
	
	
	var classhelpers =
	{
	    buildDropdown: function(result, dropdown, emptyMessage)
	    {
	        // Remove current options
	        dropdown.html('');
	        // Add the empty option with the empty message
	        dropdown.append('<option value="">' + emptyMessage + '</option>');
	        // Check result isnt empty
	        if(result != '')
	        {
	            // Loop through each of the results and append the option to the dropdown
	            $.each(result, function(k, v) {
	                dropdown.append('<option value="' + v.classId + '">' + v.className + '</option>');
	            });
	        }
	    }
	}
	
	$("#submitBrandForm").submit(function(e) {
		var frm = $('#submitBrandForm');
		e.preventDefault();

	    var data = {}
	    data["chapterName"] = $('#chapterName').val();
	    data["classId"] =  $('#secClass').val();
	    data["subjectId"] =  $('#secSubject').val();
		$.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/createChapter",
            data: JSON.stringify(data),
            dataType: 'json',
            timeout: 600000,
            success: function (data) {
                //$("#btn-update").prop("disabled", false);
                //...
            	alert("Data saved");
            },
            error: function (e) {
                //$("#btn-save").prop("disabled", false);
                //...
            	console.log("ERROR : ", e);
            }
	});
	    
	    
	});

		
});

$(document).on("change", '#secClass', function(e) {
var standard = $(this).val();
$.ajax({
    type: "GET",
    url: "/getAllSubjects",
    data: {classId: standard},
    success: function(result)
    {
    	console.log(JSON.stringify(result));
    	subjecthelpers.buildDropdown(
            jQuery.parseJSON(JSON.stringify(result)),
            $('#secSubject'),
            'Select an option'
        );
    }
});

var subjecthelpers =
{
    buildDropdown: function(result, dropdown, emptyMessage)
    {
        // Remove current options
        dropdown.html('');
        // Add the empty option with the empty message
        dropdown.append('<option value="">' + emptyMessage + '</option>');
        // Check result isnt empty
        if(result != '')
        {
            // Loop through each of the results and append the option to the dropdown
            $.each(result, function(k, v) {
                dropdown.append('<option value="' + v.subjectId + '">' + v.subjectName + '</option>');
            });
        }
    }
}
});