$( document ).ready(function(){
	
	$.ajax({
        type: "GET",
        url: "/getAllClasses",
        success: function(result)
        {
        	console.log(JSON.stringify(result));
        	classhelpers.buildDropdown(
                jQuery.parseJSON(JSON.stringify(result)),
                $('#secClass'),
                'Select an option'
            );
        }
    });
	
	
	var classhelpers =
	{
	    buildDropdown: function(result, dropdown, emptyMessage)
	    {
	        // Remove current options
	        dropdown.html('');
	        // Add the empty option with the empty message
	        dropdown.append('<option value="">' + emptyMessage + '</option>');
	        // Check result isnt empty
	        if(result != '')
	        {
	            // Loop through each of the results and append the option to the dropdown
	            $.each(result, function(k, v) {
	                dropdown.append('<option value="' + v.classId + '">' + v.className + '</option>');
	            });
	        }
	    }
	}
	
	$("#submitBrandForm").submit(function(e) {
		var frm = $('#submitBrandForm');
		e.preventDefault();

	    var data = {}
	    data["chapterName"] = $('#chapterName').val();
	    data["classId"] =  $('#secClass').val();
	    data["   "] =  $('#secSubject').val();
		$.ajax({
	        type: "POST",
	        contentType: "application/json",
	        url: "/uploadLiveSession",
	        data: JSON.stringify(data),
	        dataType: 'json',
	        timeout: 600000,
	        success: function (data) {
	            //$("#btn-update").prop("disabled", false);
	            //...
	        	alert("Data saved");
	        },
	        error: function (e) {
	            //$("#btn-save").prop("disabled", false);
	            //...
	        	console.log("ERROR : ", e);
	        }
	});
	    
	    
	});

		
});

$(document).on("change", '#secClass', function(e) {
var standard = $(this).val();
$.ajax({
    type: "GET",
    url: "/getAllSubjects",
    data: {classId: standard},
    success: function(result)
    {
    	console.log(JSON.stringify(result));
    	subjecthelpers.buildDropdown(
            jQuery.parseJSON(JSON.stringify(result)),
            $('#secSubject'),
            'Select an option'
        );
    }
});

var subjecthelpers =
{
    buildDropdown: function(result, dropdown, emptyMessage)
    {
        // Remove current options
        dropdown.html('');
        // Add the empty option with the empty message
        dropdown.append('<option value="">' + emptyMessage + '</option>');
        // Check result isnt empty
        if(result != '')
        {
            // Loop through each of the results and append the option to the dropdown
            $.each(result, function(k, v) {
                dropdown.append('<option value="' + v.subjectId + '">' + v.subjectName + '</option>');
            });
        }
    }
}


});


$(document).on("change", '#secSubject', function(e) {
	var standard = $("#secClass").val();
	var subject = $(this).val();
	$.ajax({
	    type: "GET",
	    url: "/getChaptersByClassIdSubjectId",
	    data: {classId: standard,subjectId: subject},
	    success: function(result)
	    {
	    	console.log(JSON.stringify(result));
	    	chapterhelpers.buildDropdown(
	            jQuery.parseJSON(JSON.stringify(result)),
	            $('#secChapter'),
	            'Select an option'
	        );
	    }
	});

	var chapterhelpers =
	{
	    buildDropdown: function(result, dropdown, emptyMessage)
	    {
	        // Remove current options
	        dropdown.html('');
	        // Add the empty option with the empty message
	        dropdown.append('<option value="">' + emptyMessage + '</option>');
	        // Check result isnt empty
	        if(result != '')
	        {
	            // Loop through each of the results and append the option to the dropdown
	            $.each(result, function(k, v) {
	                dropdown.append('<option value="' + v.chapterId + '">' + v.chapterName + '</option>');
	            });
	        }
	    }
	}


	});

