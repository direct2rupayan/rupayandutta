
        $( document ).ready(function(){
            var objective=$('#Objectivecontent');
            var entry='';
            $.ajax({
                url: '/getObjectives',
                type: 'GET',
                success: function(data){
                	jQuery.each(data, function(index, item) {
                       entry =  '<div class="row rounded chapter text-dark border">'
                       + '<div class="col-md-2 mb-2">' + item.serialNo + '</div>'
                       + '<div class="col-md-4 mb-2"><strong>Learn Now</strong></div>'
                       + '<div class="col-md-4 mb-2">' + item.contentName + '</div>'
                       + '<div class="col-md-2 mb-2 d-flex flex-row align-items-center justify-content-between">'
                          + ' 22th May 2020' 
                       + '<div class="dropdown no-arrow"> ' +
                       '<div class="dropdown no-arrow"><a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v fa-sm fa-fw gray-600"></i></a><div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink"><a class="dropdown-item" href="#" data-toggle="modal" data-target="#addContent">View / Edit</a> <a class="dropdown-item text-danger" href="#" data-toggle="modal" data-target="#deleteContent">Delete</a></div>'
                         + '</div></div></div>'
                       $('#Objectivecontent').append(entry);
                                     		
                   });
                	
                	
                }
            });
        });