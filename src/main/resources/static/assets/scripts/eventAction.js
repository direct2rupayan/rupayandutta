
$(document).ready(function(){
	
	var timeOpts='';
	var j=12;
	for(var i=1;i<=12;i++){
			timeOpts+="<option value='"+i+" AM'>"+i+" AM</option>";
	}
	for(var i=1;i<=12;i++){
		timeOpts+="<option value='"+i+" PM'>"+i+" PM</option>";
	}
		
	$("#schStart").append(timeOpts);
	$("#schEnd").append(timeOpts);
	$("#addSchduleBtn").show();
	$("#updSchduleBtn").hide();
	
	var url = window.location.href;
	var eventPath="";
	
	if(url.includes("school-schedule-all") || url.includes("student-schedule")){
		eventPath="/events/get/ALL";
	}
	if(url.includes("school-schedule-me") || url.includes("student-schedule-me")){
		eventPath="/events/get/ME"
	}
	
	//"/events/get",
	
	var todayDate = new Date().toISOString().slice(0,10);
	var e=document.getElementById("calendar");
		new FullCalendar.Calendar(e,
			{plugins:["interaction","dayGrid","timeGrid","list"],
		header:{left:"prev,next today",center:"title",right:"dayGridMonth,timeGridWeek,timeGridDay,listMonth"},
		defaultDate:todayDate,
		navLinks:!0,selectable:!0,selectMirror:!0,editable:!0,eventLimit:!0,
		events:eventPath,
		eventClick:function (info){
			//alert(info.event.id +" "+info.event.title+" "+info.event.extendedProps.grade+" "+info.event.start+" "+info.event.end);
			$("#schType").val(info.event.classNames);
			$("#schTitle").val(info.event.title);
			var date= moment(new Date(info.event.start)).format('DD/MM/YYYY');
			var stTime= moment(new Date(info.event.start)).format('h A');
			var endTime= moment(new Date(info.event.end)).format('h A');
			//alert(date+" "+stTime+" "+endTime);
			$("#schDate").val(date);
			$("#schStart").val(stTime);
			$("#schEnd").val(endTime);
			$("#schGrade").val(info.event.extendedProps.grade);
			$("#schSubject").val(info.event.extendedProps.subject);
			$("#schChapter").val(info.event.extendedProps.chapter);
			$("#schRemarks").val(info.event.extendedProps.remarks);
			$("#mod_cal_eventId").val(info.event.id);
		}
		}).render();
		
		
	
});

function createUpdateScheduleEvent(flag){
	
	/*$("#addSchduleBtn").show();
	$("#updSchduleBtn").show();*/
	
	//alert($("#schType").val() +"|"+$("#schTitle").val() +"|"+$("#schDate").val() +"|"+$("#schStart").val() +"|"+$("#schEnd").val() +"|"+$("#schGrade").val());
	
	if(flag!="DEL"){
	
		if($("#schType").val()===undefined || $("#schType").val()=='' ||
			$("#schTitle").val()===undefined || $("#schTitle").val()=='' ||
			$("#schDate").val()===undefined || $("#schDate").val()=='' ||
			$("#schStart").val()===undefined || $("#schStart").val()=='' ||
			$("#schEnd").val()===undefined || $("#schEnd").val()=='' ||
			$("#schGrade").val()===undefined || $("#schGrade").val()==''||
			$("#schRemarks").val()===undefined || $("#schRemarks").val()==''){
		
		alert("Please fill the data for mandatory fields.");
		return false;
		}
	}
	
	var data = {};
	
	data["schType"]=$("#schType").val();
	data["schTitle"]=$("#schTitle").val();
	data["schDate"]=$("#schDate").val();
	data["schStart"]=$("#schStart").val();
	data["schEnd"]=$("#schEnd").val();
	data["schGrade"]=$("#schGrade").val();
	data["schSubject"]=$("#schSubject").val();
	data["schChapter"]=$("#schChapter").val();
	data["schRemarks"]=$("#schRemarks").val();
	data["loginName"]=$("#loginName").val();
	data["eventID"]=$("#mod_cal_eventId").val();
	
	
	//alert (JSON.stringify(data));
	
	var defUrl="";var defMethod="";
		
	if(flag=="ADD"){
		defUrl="/events/create";defMethod="POST";
	}
	if(flag=="UPD"){
		defUrl="/events/modify";defMethod="PUT";
	}
	if(flag=="DEL"){
	defUrl="/events/delete";defMethod="DELETE";
	}
	
	$.ajax({
        type: defMethod,
        contentType: "application/json",
        url: defUrl,
        data: JSON.stringify(data),
        dataType: 'json',
        timeout: 600000,
        statusCode: {
            417: function(responseObject, textStatus, jqXHR) {
            	alert("Failed to add event.");
            },
            201: function(responseObject, textStatus, errorThrown) {
               alert("Event added successfully.");
            },
            200: function(responseObject, textStatus, errorThrown) {
                alert("Event changes are successfully saved.");
            }
        }
    });
	
	/*$("#addSchduleBtn").hide();
	$("#updSchduleBtn").show();*/
	
}