
$(document).ready(function(){
	//alert("Hula POPO");
	$("#updateBtn").hide();
	$("#updateBtnIns").hide();
});

function checkSelected(){
	
	var stdId=$('input:radio[name=selectStudent]:checked').val();
	//alert("stdId:"+stdId+":");
	if(stdId === undefined){
	alert("Please select a radio to proceed.");
	$("#operationDiv").hide();
	return false;
	}else{
		$("#operationDiv").show();
	}
}

function callSaveStudent(){
	//alert("Hi callSaveStudent is Called");
	
	if($("#enrollmentNo").val()===undefined || $("#enrollmentNo").val()=='' ||
			$("#fullname").val()===undefined || $("#fullname").val()=='' ||
			$("#email").val()===undefined || $("#email").val()==''){
		
		alert("Please fill Enrollment number,name,email data.");
		return false;
		}
	
	if($("#enrollmentNo").val())
	
	var data = {}
    
	data["enrollmentNumber"] = $("#enrollmentNo").val();
    data["name"] = $("#fullname").val();
    data["dtOfBirth"] = $("#dob").val();
    data["grade"] = $("#grade").val();
    data["section"] = $("#section").val();
    data["bloodGr"] = $("#bloodgroup").val();
    data["nationality"] = $("#nationality").val();
    data["religion"] = $("#religion").val();
    data["category"] = $("#category").val();
    data["email"] = $("#email").val();
    data["phone"] = $("#phone").val();
    data["status"] = 1;
    
    data["currentStreetAddress"] = $("#addresslineone").val();
    data["currentPostOffice"] = $("#postoffice").val();
    data["currentPoliceStation"] = $("#plicestation").val();
    data["currentPIN"] = $("#pincode").val();
    
    data["permStreetAddress"] = $("#peraddresslineone").val();
    data["permPostOffice"] = $("#perpostoffice").val();
    data["permPoliceStation"] = $("#perplicestation").val();
    data["permPIN"] = $("#perpincode").val();
    
    data["pName"] = $("#parentfullname").val();
    data["pRelation"] = $("#relation").val();
    data["pOccupation"] = $("#occupation").val();
    data["pEmail"] = $("#e-mail").val();
    data["pPhone"] = $("#phone").val();
    data["gender"] = $("#gender").val();
    
    data["sibEnrollmentNum"] = $("#sibenrollmentNo").val();
    data["sibName"] = $("#sibfullname").val();
    data["sibGrade"] = $("#sibgrade").val();
    data["sibSection"] = $("#sibsection").val();
    
    data["aadhar"] = $("#aadhaar").val();
    
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/student/create",
        data: JSON.stringify(data),
        dataType: 'json',
        timeout: 600000,
        statusCode: {
            404: function(responseObject, textStatus, jqXHR) {
            	
            },
            201: function(responseObject, textStatus, errorThrown) {
                alert("Student details added successfully.")
                location.reload(true);
            }           
        }
    });
	
   	
}

function callSaveInstructor(){
	//alert("Hi callSaveStudent is Called");
	
	$("#updateBtnIns").hide();
	$("#savBtnIns").show();
	
	if($("#emplNo").val()===undefined || $("#emplNo").val()=='' ||
			$("#fullname").val()===undefined || $("#fullname").val()=='' ||
			$("#email").val()===undefined || $("#email").val()==''){
		
		alert("Please fill Emp No,name,email data.");
		return false;
		}
	
    
	var data = {};
    
	data["employmentNumber"] = $("#emplNo").val();
    data["name"] = $("#fullname").val();
    data["dtOfBirth"] = $("#dob").val();
    data["teachingInGrades"] = $("#techGrade").val();
    data["secondarySubjects"] = $("#secSubjects").val().toString();
    data["primarySubjects"] = $("#primarySubjects").val().toString();
    data["bloodGr"] = $("#bloodgroup").val();
    data["nationality"] = $("#nationality").val();
    data["religion"] = $("#religion").val();
    data["category"] = $("#category").val();
    data["email"] = $("#email").val();
    data["phone"] = $("#phone").val();
    data["currentStreetAddress"] = $("#addresslineone").val();
    data["currentPostOffice"] = $("#postoffice").val();
    data["currentPoliceStation"] = $("#plicestation").val();
    data["currentPIN"] = $("#pincode").val();
    data["permStreetAddress"] = $("#peraddresslineone").val();
    data["permPostOffice"] = $("#perpostoffice").val();
    data["permPoliceStation"] = $("#perplicestation").val();
    data["permPIN"] = $("#perpincode").val();
    data["gender"] = "M";
    data["status"] = 1;
    data["aadhar"]=$("#aadhaar").val();
    
    //alert ("data changed :"+JSON.stringify(data));
    
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/instructor/create",
        data: JSON.stringify(data),
        dataType: 'json',
        timeout: 600000,
        statusCode: {
            404: function(responseObject, textStatus, jqXHR) {
            	
            },
            201: function(responseObject, textStatus, errorThrown) {
                alert("Instructor details added successfully.")
                location.reload(true);
            }           
        }
    });
	
   	
}

function callPopulateStudentData(){
	
	//alert("Retreive Data forEdit is called");
	
	var stdId=$('input:radio[name=selectStudent]:checked').val();
	
	//alert("Student ID retreived :"+stdId);
	
	if(stdId === undefined)
		return false;
	
	$("#updateBtn").show();
	$("#savBtn").hide();
	
	$.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/student/"+stdId,
        dataType: 'json',
        timeout: 600000,
        statusCode: {
            404: function(responseObject, textStatus, jqXHR) {
            	
            },
            200: function(responseObject, textStatus, errorThrown) {
                //alert("Student Retreived Successfully :"+responseObject.enrollmentNumber);
               
                $("#enrollmentNo").val(responseObject.enrollmentNumber);
                $("#fullname").val(responseObject.name);
                $("#dob").val(responseObject.dtOfBirth);
                $("#grade").val(responseObject.grade);
                $("#section").val(responseObject.section);
                $("#bloodgroup").val(responseObject.bloodGr);
                $("#nationality").val(responseObject.nationality);
                $("#religion").val(responseObject.religion);
                $("#category").val(responseObject.category);
                $("#email").val(responseObject.email);
                $("#phone").val(responseObject.phone);
                $("#addresslineone").val(responseObject.currentStreetAddress);
                $("#postoffice").val(responseObject.currentPostOffice);
                $("#plicestation").val(responseObject.currentPoliceStation);
                $("#pincode").val(responseObject.currentPIN);
                $("#peraddresslineone").val(responseObject.permStreetAddress);
                $("#perpostoffice").val(responseObject.permPostOffice);
                $("#perplicestation").val(responseObject.permPoliceStation);
                $("#perpincode").val(responseObject.permPIN);
                $("#parentfullname").val(responseObject.pName);
                $("#relation").val(responseObject.pRelation);
                $("#occupation").val(responseObject.pOccupation);
                $("#e-mail").val(responseObject.pEmail);
                $("#phone").val(responseObject.pPhone);
                console.log(responseObject.gender);
                var q= responseObject.gender;
                $("#gender").val(q);
                $("#sibenrollmentNo").val(responseObject.sibEnrollmentNum);
                $("#sibfullname").val(responseObject.sibName);
                $("#sibgrade").val(responseObject.sibGrade);
                $("#sibsection").val(responseObject.sibSection);

            }           
        }
    });
}

function callPopulateInstructorData(){
	
	//alert("Retreive Data forEdit is called");
	
	var instId=$('input:radio[name=selectTutor]:checked').val();
	
	//alert("Instructor ID retreived :"+instId);
	
	if(instId === undefined)
		return false;
	
	$("#updateBtnIns").show();
	$("#savBtnIns").hide();
	
	$.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/instructor/"+instId,
        dataType: 'json',
        timeout: 600000,
        statusCode: {
            404: function(responseObject, textStatus, jqXHR) {
            	
            },
            200: function(responseObject, textStatus, errorThrown) {
                alert("Instructor Retreived Successfully :"+responseObject.employmentNumber);
               
                $("#emplNo").val(responseObject.employmentNumber);
                $("#fullname").val(responseObject.name);
                $("#dob").val(responseObject.dtOfBirth);
                $("#bloodgroup").val(responseObject.bloodGr);
                $("#nationality").val(responseObject.nationality);
                $("#religion").val(responseObject.religion);
                $("#category").val(responseObject.category);
                $("#email").val(responseObject.email);
                $("#phone").val(responseObject.phone);
                $("#primarySubjects").val(responseObject.primarySubjects);
                $("#secSubjects").val(responseObject.secondarySubjects);
                $("#techGrade").val(responseObject.teachingInGrades);
                $("#addresslineone").val(responseObject.currentStreetAddress);
                $("#postoffice").val(responseObject.currentPostOffice);
                $("#plicestation").val(responseObject.currentPoliceStation);
                $("#pincode").val(responseObject.currentPIN);
                $("#peraddresslineone").val(responseObject.permStreetAddress);
                $("#perpostoffice").val(responseObject.permPostOffice);
                $("#perplicestation").val(responseObject.permPoliceStation);
                $("#perpincode").val(responseObject.permPIN);
                $("#gender").val(responseObject.gender);
                $("#aadhaar").val(responseObject.aadhar)
            }           
        }
    });
}

function callUpdateStudent(){
	
	//alert("call for Editing Data");
	
	if($("#enrollmentNumber").val()===undefined || $("#enrollmentNumber").val()=='' ||
			$("#fullname").val()===undefined || $("#fullname").val()=='' ||
			$("#email").val()===undefined || $("#email").val()==''){
		
		alert("Please fill Enrollment number,name,email data.");
		return false;
		}
	
	var stdId=$('input:radio[name=selectStudent]:checked').val();
	
	if(stdId === undefined)
		return false;
	
	var data = {}
    
	data["enrollmentNumber"] = $("#enrollmentNo").val();
    data["name"] = $("#fullname").val();
    data["dtOfBirth"] = $("#dob").val();
    data["grade"] = $("#grade").val();
    data["section"] = $("#section").val();
    data["bloodGr"] = $("#bloodgroup").val();
    data["nationality"] = $("#nationality").val();
    data["religion"] = $("#religion").val();
    data["category"] = $("#category").val();
    data["email"] = $("#email").val();
    data["phone"] = $("#phone").val();
    data["gender"] = $("#gender").val();
    
    data["currentStreetAddress"] = $("#addresslineone").val();
    data["currentPostOffice"] = $("#postoffice").val();
    data["currentPoliceStation"] = $("#plicestation").val();
    data["currentPIN"] = $("#pincode").val();
    
    data["permStreetAddress"] = $("#peraddresslineone").val();
    data["permPostOffice"] = $("#perpostoffice").val();
    data["permPoliceStation"] = $("#perplicestation").val();
    data["permPIN"] = $("#perpincode").val();
    
    data["pName"] = $("#parentfullname").val();
    data["pRelation"] = $("#relation").val();
    data["pOccupation"] = $("#occupation").val();
    data["pEmail"] = $("#e-mail").val();
    data["pPhone"] = $("#pphone").val();
    
    data["sibEnrollmentNum"] = $("#sibenrollmentNo").val();
    data["sibName"] = $("#sibfullname").val();
    data["sibGrade"] = $("#sibgrade").val();
    data["sibSection"] = $("#sibsection").val();
    data["aadhar"]=$("#aadhaar").val();
	
	$.ajax({
        type: "PUT",
        contentType: "application/json",
        url: "/student/"+stdId,
        data: JSON.stringify(data),
        dataType: 'json',
        timeout: 600000,
        statusCode: {
            404: function(responseObject, textStatus, jqXHR) {
            	
            },
            200: function(responseObject, textStatus, errorThrown) {
                alert("Student details updated successfully.");
                location.reload(true);
            }           
        }
    });
	
}

function callUpdateInstructor(){
	
	if($("#emplNo").val()===undefined || $("#emplNo").val()=='' ||
			$("#fullname").val()===undefined || $("#fullname").val()=='' ||
			$("#email").val()===undefined || $("#email").val()==''){
		
		alert("Please fill Emp No,name,email data.");
		return false;
		}
	
	var instId=$('input:radio[name=selectTutor]:checked').val();
	
	alert("Instructor ID retreived :"+instId);
	
	if(instId === undefined)
		return false;
	
	$("#updateBtnIns").show();
	$("#savBtnIns").hide();
	
    
	var data = {};
    
	data["employmentNumber"] = $("#emplNo").val();
    data["name"] = $("#fullname").val();
    data["dtOfBirth"] = $("#dob").val();
    data["teachingInGrades"] = $("#techGrade").val();
    data["secondarySubjects"] = $("#secSubjects").val().toString();
    data["primarySubjects"] = $("#primarySubjects").val().toString();
    data["bloodGr"] = $("#bloodgroup").val();
    data["nationality"] = $("#nationality").val();
    data["religion"] = $("#religion").val();
    data["category"] = $("#category").val();
    data["email"] = $("#email").val();
    data["phone"] = $("#phone").val();
    data["currentStreetAddress"] = $("#addresslineone").val();
    data["currentPostOffice"] = $("#postoffice").val();
    data["currentPoliceStation"] = $("#plicestation").val();
    data["currentPIN"] = $("#pincode").val();
    data["permStreetAddress"] = $("#peraddresslineone").val();
    data["permPostOffice"] = $("#perpostoffice").val();
    data["permPoliceStation"] = $("#perplicestation").val();
    data["permPIN"] = $("#perpincode").val();
    data["gender"] = "M";
    data["status"] = 1;
    data["aadhar"]=$("#aadhaar").val();
    
    alert ("data changed :"+JSON.stringify(data));
    
    $.ajax({
        type: "PUT",
        contentType: "application/json",
        url: "/instructor/"+instId,
        data: JSON.stringify(data),
        dataType: 'json',
        timeout: 600000,
        statusCode: {
            404: function(responseObject, textStatus, jqXHR) {
            	
            },
            200: function(responseObject, textStatus, errorThrown) {
                alert("Instructor details updated successfully.");
                location.reload(true);
            }           
        }
    });
	
}

function callDeleteStudent(){
	
	//alert("Retreive Data forDelete is called");
	
	var stdId=$('input:radio[name=selectStudent]:checked').val();
	
	//alert("Student ID retreived :"+stdId);
	
	if(stdId === undefined)
		return false;
	
	$.ajax({
        type: "DELETE",
        contentType: "application/json",
        url: "/student/"+stdId,
        dataType: 'json',
        timeout: 600000,
        statusCode: {
            404: function(responseObject, textStatus, jqXHR) {
            	
            },
            200: function(responseObject, textStatus, errorThrown) {
                alert("Student details deleted successfully.");
                location.reload(true);	
            }           
        }
    });
	
}

function callDeleteInstructor(){
	
	//alert("Retreive Data forDelete is called");
	
	var stdId=$('input:radio[name=selectTutor]:checked').val();
	
	//alert("Student ID retreived :"+stdId);
	
	if(stdId === undefined)
		return false;
	
	$.ajax({
        type: "DELETE",
        contentType: "application/json",
        url: "/instructor/"+stdId,
        dataType: 'json',
        timeout: 600000,
        statusCode: {
            404: function(responseObject, textStatus, jqXHR) {
            	
            },
            200: function(responseObject, textStatus, errorThrown) {
                alert("Instructor details deleted successfully.");
                location.reload(true);	
            }           
        }
    });
	
}

function callChangeInstructorStatus(){
	//alert("In Call Change Student Status");
	
	var stdId=$('input:radio[name=selectTutor]:checked').val();
	
	//alert("Student ID retreived :"+stdId);
	
	if(stdId === undefined)
		return false;
	
	var rowdata= $("#userList input:radio[name=selectTutor]:checked").closest('tr').find("td:nth-child(7)").text().trim();
	alert(rowdata);
	
	$.ajax({
        type: "PATCH",
        contentType: "application/json",
        url: "/instructor/"+stdId,
        data: '{"status":"'+rowdata+'"}',
        dataType: 'json',
        timeout: 600000,
        statusCode: {
            404: function(responseObject, textStatus, jqXHR) {
            	
            },
            200: function(responseObject, textStatus, errorThrown) {
                alert("Instructor status updated successfully.");
                location.reload(true);
            }           
        }
    });
}

function callChangeStudentStatus(){
	//alert("In Call Change Student Status");
	
	var stdId=$('input:radio[name=selectStudent]:checked').val();
	
	//alert("Student ID retreived :"+stdId);
	
	if(stdId === undefined)
		return false;
	
	var rowdata= $("#userList input[name=selectStudent]:checked").closest('tr').find("td:nth-child(9)").text().trim();
	alert(rowdata);
	
	$.ajax({
        type: "PATCH",
        contentType: "application/json",
        url: "/student/"+stdId,
        data: '{"status":"'+rowdata+'"}',
        dataType: 'json',
        timeout: 600000,
        statusCode: {
            404: function(responseObject, textStatus, jqXHR) {
            	
            },
            200: function(responseObject, textStatus, errorThrown) {
                alert("Student status updated successfully.");
                location.reload(true);
            }           
        }
    });
}

function checkValidSibling(){
	alert("checkValidSibling is called");
	var name=$('#sibfullname').val().trim();
	var grade=$('#sibgrade').val();
	if(name !== undefined && grade !== undefined && name != '' && grade != ''){
		
		var data = {}
	    
		data["sibname"] = name;
	    data["sibgrade"] = grade;
	    
	    alert (JSON.stringify(data));
		
		$.ajax({
	        type: "POST",
	        contentType: "application/json",
	        url: "/student/siblinginfo",
	        data: JSON.stringify(data),
	        dataType: 'json',
	        timeout: 600000,
	        statusCode: {
	            404: function(responseObject, textStatus, jqXHR) {
	            	$("#sibenrollmentNo").val("");
	                $("#sibfullname").val("");
	                $("#sibgrade").val("");
	                $("#sibsection").val("");
	            },
	            200: function(responseObject, textStatus, errorThrown) {
	                alert("Sibling record found");//: "+JSON.stringify(responseObject));
	                $("#sibenrollmentNo").val(responseObject.enrollmentNumber);
	                $("#sibfullname").val(responseObject.name);
	                $("#sibgrade").val(responseObject.grade);
	                $("#sibsection").val(responseObject.section);
	            }           
	        }
	    });
	}
}
