'use strict';

jQuery(function () {
  // Desktop to Responsive header
  jQuery('.main-menu').clone().insertAfter('.jquery-accordion-menu .jquery-accordion-menu-header'); // Desktop sticky header

  jQuery('.d-topmenu').clone().appendTo('.header-sticky .container');
  jQuery('.responsive-menu').clone().insertAfter('.header-sticky .container');
});
jQuery(document).ready(function () {
  // Mobile menu
  jQuery(document).ready(function () {
    jQuery('#jquery-accordion-menu').jqueryAccordionMenu();
    jQuery('.colors a').click(function () {
      if ($(this).attr('class') != 'default') {
        jQuery('#jquery-accordion-menu').removeClass();
        jQuery('#jquery-accordion-menu').addClass('jquery-accordion-menu').addClass($(this).attr('class'));
      } else {
        jQuery('#jquery-accordion-menu').removeClass();
        jQuery('#jquery-accordion-menu').addClass('jquery-accordion-menu');
      }
    });
  }); // On scroll header

  jQuery('.header-sticky').hide();
  jQuery(window).scroll(function () {
    if (jQuery(document).scrollTop() > 80) {
      jQuery('.header-sticky').slideDown('fast');
    } else {
      jQuery('.header-sticky').slideUp('fast');
    }
  }); // Slide menu on responsive

  jQuery('.slide-menu').click(function () {
    jQuery('.responsive-menu').addClass('highlight');
  });
  jQuery('.close-slide-menu').click(function () {
    jQuery('.responsive-menu').removeClass('highlight');
  }); // Footer menu on responsive

  if ($(window).width() < 571) {
    $('.toggle').click(function (e) {
      e.preventDefault();
      var $this = $(this);

      if ($this.next().hasClass('show')) {
        $this.next().removeClass('show');
        $this.next().slideUp(350);
      } else {
        $this.parent().parent().find('li .inner').removeClass('show');
        $this.parent().parent().find('li .inner').slideUp(350);
        $this.next().toggleClass('show');
        $this.next().slideToggle(350);
      }
    });
  } // Show and Hide password


  jQuery('.password-icon').click(function () {
    if ($('#loginpassword').attr('type') == 'text') {
      $('#loginpassword').attr('type', 'password');
      $('.password-icon').toggleClass('show');
    } else {
      $('#loginpassword').attr('type', 'text');
      $('.password-icon').toggleClass('show');
    }
  });
}); // Login step form
// $(document).ready(function(){
//     $(".new-account").click(function(){
//         $("#login .close").trigger("click");
//     });
//     $(".login-account").click(function(){
//         $("#free-registration .close").trigger("click");
//     });
// });
// Bottom message

$('.fixed-message-closed').click(function () {
  $('.fixed-message').toggleClass('fixed-message-slideup');
  $('.fixed-message-closed').toggleClass('fixed-message-rotate');
}); // Marquee on Bottom

(function () {
  function start_marquee() {
    function go() {
      i = i < width ? i + step : 1;
      m.style.marginLeft = -i + 'px';
    }

    var i = 0,
        step = 3,
        space = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    var m = document.getElementById('marquee');
    var t = m.innerHTML; //text

    m.innerHTML = t + space;
    m.style.position = 'absolute'; // http://stackoverflow.com/questions/2057682/determine-pixel-length-of-string-in-javascript-jquery/2057789#2057789

    var width = m.clientWidth + 1;
    m.style.position = '';
    m.innerHTML = t + space + t + space + t + space + t + space + t + space + t + space + t + space;

    if (m.addEventListener) {
      m.addEventListener('mouseenter', function () {
        step = 0;
      }, false);
      m.addEventListener('mouseleave', function () {
        step = 3;
      }, false);
    }

    var x = setInterval(go, 50);
  }

  if (window.addEventListener) {
    window.addEventListener('load', start_marquee, false);
  } else if (window.attachEvent) {
    //IE7-8
    window.attachEvent('onload', start_marquee);
  }
})(); // Banner Slider


$(document).ready(function () {
  // Home page header slider
  var owl = $('.home-banner');
  owl.owlCarousel({
    items: 1,
    loop: true,
    smartSpeed: 700,
    margin: 20,
    autoplay: true,
    autoplayTimeout: 2500,
    autoplayHoverPause: true,
    // animateIn: 'fadeIn', // add this
    animateOut: 'fadeOut' // and this

  }); // $('.play').on('click', function() {
  //     owl.trigger('play.owl.autoplay', [2500])
  // })
  // $('.stop').on('click', function() {
  //     owl.trigger('stop.owl.autoplay')
  // })
  // Testimonial slider

  var owlts = $('.testimonial-carousel');
  owlts.owlCarousel({
    items: 1,
    loop: true,
    smartSpeed: 700,
    margin: 20,
    //autoplay: true,
    autoplayTimeout: 2500,
    autoplayHoverPause: true,
    // animateIn: 'fadeIn', // add this
    animateOut: 'fadeOut',
    // and this
    responsive: {
      767: {
        items: 2
      }
    }
  });
}); // On click change youtube link on iframe

jQuery(document).ready(function () {
  jQuery('.lightbox-video').on('click', function () {
    var videoId = jQuery(this).attr('video-id');
    var videoUrl = 'https://www.youtube.com/embed/' + videoId;
    jQuery('#videobox').attr('src', videoUrl);
  });
});