'use strict';

(function ($) {
  'use strict'; // Start of use strict
  // Toggle the side navigation

  $('#sidebarToggle, #sidebarToggleTop').on('click', function (e) {
    $('body').toggleClass('sidebar-toggled');
    $('.sidebar').toggleClass('toggled');

    if ($('.sidebar').hasClass('toggled')) {
      $('.sidebar .collapse').collapse('hide');
    }

    ;
  }); // Close any open menu accordions when window is resized below 768px

  $(window).resize(function () {
    if ($(window).width() < 768) {
      $('.sidebar .collapse').collapse('hide');
    }

    ;
  }); // Prevent the content wrapper from scrolling when the fixed side navigation hovered over

  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function (e) {
    if ($(window).width() > 768) {
      var e0 = e.originalEvent,
          delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  }); // Scroll to top button appear

  $(document).on('scroll', function () {
    var scrollDistance = $(this).scrollTop();

    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  }); // Smooth scrolling using jQuery easing

  $(document).on('click', 'a.scroll-to-top', function (e) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: $($anchor.attr('href')).offset().top
    }, 1000, 'easeInOutExpo');
    e.preventDefault();
  });
})(jQuery); // End of use strict
// Add tooltip


$(document).ready(function () {
  $('[data-toggle="tooltip"]').tooltip();
}); // Add same height

$('a').click(function () {
  setTimeout(function () {
    jQuery('.js-height-same').matchHeight();
  }, 40);
}); // Match height

jQuery('.js-height-same').matchHeight(); // Student dashboard my progress chart changes 

$('.change-report').on('click', function (e) {
  $('.p-chart').toggleClass('hide'); // button text changes

  if ($(this).text() == 'Performance') $(this).text('Progress');else $(this).text('Performance'); // button text changes

  if ($('.progress-heading').text() == 'Progress Overview') $('.progress-heading').text('Performance Overview');else $('.progress-heading').text('Progress Overview');
}); // School dashboard search filter

$(function () {
  $('#school-viewrole').change(function () {
    $('.search-controll').hide();
    $('.' + $(this).val()).show();
  });
}); // Sent Notification filter

$(function () {
  $('#sendnotitype').change(function () {
    $('.notiStuoption').hide();
    $('.' + $(this).val()).show();
  });
}); // Change URL as per select

$('select#changeurl').bind('change', function () {
  // bind change event to select
  //var url = $('select#changeurl').val() + '.html'; // get selected value
  var url = $('select#changeurl').val() ; // removing the html part as it will be taken care by controller

  if (url != '') {
    // require a URL
    window.location = url; // redirect
  }

  return false;
}); // Date Picker

$('.datepicker').datepicker({
  uiLibrary: 'bootstrap4'
}); // Date Picker for label

$(document).ready(function () {
  $('input.datepicker').click(function () {
    // console.log('Hello world!');
    $(this).parent().addClass('focus');
  });
}); // Image upload with display

$(document).ready(function () {
  $('.file-upload').file_upload();
}); // Drag & Drop section

$(function () {
  $('.sortable').sortable();
  $('.sortable').disableSelection();
}); // Custom Select

$(document).ready(function () {
  $('.customSelect').select2();
  $('.customSelect-multiple').select2({
    placeholder: 'Select Multiple Options',
    allowClear: true
  });
}); // Custom Scroll Bar

(function ($) {
  $(window).on('load', function () {
    $('.custom-scroll').mCustomScrollbar({
      //setHeight:340,
      theme: 'minimal-dark'
    });
  });
})(jQuery); // Custom Scroll Bar for select2


$('select').on('select2:open', function (e) {
  $('.select2-results__options').mCustomScrollbar('destroy');
  setTimeout(function () {
    //$('.select2-results__options').mCustomScrollbar();
    $('.select2-results__options').mCustomScrollbar({
      //setHeight:260,
      theme: 'minimal-dark'
    });
  }, 0);
});