// Progress bar width and color as per the number
function init() {

  $('.progress').each(function () {
    var $this = $(this);
    var progressValue = $this.children().attr('aria-valuenow');

    $this.children().width(progressValue + "%");
    progressValue++;

    if(progressValue >= "80") {
      console.log("80+");
      $(this).children().addClass('bg-success');
    } else if(progressValue >= "60") {
      $(this).children().addClass('bg-primary');
    } else if(progressValue >= "45") {
      $(this).children().addClass('bg-info');
    } else if(progressValue >= "30") {
      $(this).children().addClass('bg-warning');
    } else {
      $(this).children().addClass('bg-danger');
    };

  })

}init();