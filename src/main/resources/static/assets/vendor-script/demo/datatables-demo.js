// Call the dataTables jQuery plugin
$(document).ready(function() {
  $('#dashboardEnqury').DataTable( {
	   autoFill: true,
	   "pageLength": 3,
	   "lengthMenu": [[3, 10, 20], [3, 10, 20]],
	});
});


$(document).ready(function () {
	$('#userList').DataTable( {
	   autoFill: true,
	   "pageLength": 25,
	   "lengthMenu": [[25, 50, 100], [25, 50, 100]]
	});

   $('body').on('click', '#selectAll', function () {
      if ($(this).hasClass('allChecked')) {
         $('input[type="checkbox"]', '#userList').prop('checked', false);
      } else {
       $('input[type="checkbox"]', '#userList').prop('checked', true);
      }
       $(this).toggleClass('allChecked');
    });

   // Multi user action
   $('#userList input[type="checkbox"]').on('click', function (){
   		if ($(this).is(':checked')) {
	        $('#userList .select-menu').removeClass('hide');
	    } else {
	       $('#userList .select-menu').addClass('hide');
	    }
   });
});
