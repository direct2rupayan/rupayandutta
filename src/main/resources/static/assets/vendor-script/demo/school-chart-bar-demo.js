// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Poppins', 'sans-serif', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

function number_format(number, decimals, dec_point, thousands_sep) {
  // *     example: number_format(1234.56, 2, ',', ' ');
  // *     return: '1 234,56'
  number = (number + '').replace(',', '').replace(' ', '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}


// Color for numbers
var chartColors = {
  red: 'rgb(255, 63, 79)',
  yellow: 'rgb(250, 177, 8)',
  blueone: 'rgb(54, 185, 204)',
  bluetwo: 'rgb(70, 131, 199)',
  greenone: 'rgb(121, 202, 139)'
};



// Bar Chart One 
var ctx = document.getElementById("schoolStudentChart").getContext("2d");
var schoolStudentChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    datasets: [{
      label: '',
      backgroundColor: [
        chartColors.red,
        chartColors.red,
        chartColors.red,
        chartColors.red,
        chartColors.red,
        chartColors.red
      ],
      hoverBackgroundColor: "#2e59d9",
      borderColor: "#4e73df",
      data: [48, 55, 53, 62, 63, 68, 45, 43, 51, 56, 69, 72],
    }],
  },
  options: {
    maintainAspectRatio: false,
    layout: {
      padding: {
        left: 10,
        right: 25,
        top: 25,
        bottom: 0
      }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'Subjects'
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          maxTicksLimit: 12
        },
        maxBarThickness: 40,
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: 100,
          maxTicksLimit: 10,
          padding: 10,
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return number_format(value) + '';
          }
        },
        gridLines: {
          color: "rgb(234, 236, 244)",
          zeroLineColor: "rgb(234, 236, 244)",
          drawBorder: false,
          borderDash: [2],
          zeroLineBorderDash: [2]
        }
      }],
    },
    legend: {
      display: false
    },
    tooltips: {
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      backgroundColor: "rgb(0,0,0, 0.9)",
      bodyFontColor: "#fff",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel + number_format(tooltipItem.yLabel) + '% of Completion';
        }
      }
    },
  }

});


// Change color as per numbers
var colorChangeValue = 50; //set this to whatever is the deciding color change value
var dataset = schoolStudentChart.data.datasets[0];
for (var i = 0; i < dataset.data.length; i++) {
  if (dataset.data[i] < [30]) {
    dataset.backgroundColor[i] = chartColors.red;
  }
  else if (dataset.data[i] < [45]) {
    dataset.backgroundColor[i] = chartColors.yellow;
  }
  else if (dataset.data[i] < [60]) {
    dataset.backgroundColor[i] = chartColors.blueone;
  }
  else if (dataset.data[i] < [80]) {
    dataset.backgroundColor[i] = chartColors.bluetwo;
  }
  else if (dataset.data[i] < [101]) {
    dataset.backgroundColor[i] = chartColors.greenone;
  }
}
schoolStudentChart.update();





// Bar Chart Two 
var ctx = document.getElementById("schoolInstructorChart").getContext("2d");
var schoolInstructorChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["Sci", "Mat", "Geo", "His", "Eco"],
    datasets: [{
      label: 'Marks',
      backgroundColor: [
        chartColors.red,
        chartColors.red,
        chartColors.red,
        chartColors.red,
        chartColors.red,
        chartColors.red
      ],
      hoverBackgroundColor: "#2e59d9",
      borderColor: "#4e73df",
      data: [82, 74, 66, 59, 73],
    }],
  },
  options: {
    maintainAspectRatio: false,
    layout: {
      padding: {
        left: 10,
        right: 25,
        top: 25,
        bottom: 0
      }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'Subjects'
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          maxTicksLimit: 12
        },
        maxBarThickness: 25,
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: 100,
          maxTicksLimit: 10,
          padding: 10,
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return ' ' + number_format(value);
          }
        },
        gridLines: {
          color: "rgb(234, 236, 244)",
          zeroLineColor: "rgb(234, 236, 244)",
          drawBorder: false,
          borderDash: [2],
          zeroLineBorderDash: [2]
        }
      }],
    },
    legend: {
      display: false
    },
    tooltips: {
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      backgroundColor: "rgb(0,0,0, 0.9)",
      bodyFontColor: "#fff",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
        }
      }
    },
  }

});


// Change color as per numbers
var colorChangeValue = 50; //set this to whatever is the deciding color change value
var dataset = schoolInstructorChart.data.datasets[0];
for (var i = 0; i < dataset.data.length; i++) {
  if (dataset.data[i] < [30]) {
    dataset.backgroundColor[i] = chartColors.red;
  }
  else if (dataset.data[i] < [45]) {
    dataset.backgroundColor[i] = chartColors.yellow;
  }
  else if (dataset.data[i] < [60]) {
    dataset.backgroundColor[i] = chartColors.blueone;
  }
  else if (dataset.data[i] < [80]) {
    dataset.backgroundColor[i] = chartColors.bluetwo;
  }
  else if (dataset.data[i] < [101]) {
    dataset.backgroundColor[i] = chartColors.greenone;
  }
}
schoolInstructorChart.update();