package com.mstar.web.configuration;

import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.stereotype.Component;

import com.mstar.web.GlobalConstants.Constants;


@Component
public class SimpleAuthenticationSuccessHandler implements AuthenticationSuccessHandler  {
	Logger logger = LoggerFactory.getLogger(SimpleAuthenticationSuccessHandler.class);
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, 
		      HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		logger.info("Authentication Succcess For: " + authentication.getPrincipal());
		response.setStatus(HttpServletResponse.SC_OK);
		redirectStrategy.sendRedirect(request, response, determineTargetUrl(authentication));
	}
	
	protected String determineTargetUrl(final Authentication authentication) {
	    final Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
	    List<String> roles = new ArrayList<String>();

        for (GrantedAuthority a : authorities) {
            roles.add(a.getAuthority());
        }
	    String defaultTargetURL=Constants.ROLE_GUEST_HOME;
	    
	    if(roles.contains(Constants.ROLE_ADMIN)) {
	    	defaultTargetURL=Constants.ROLE_ADMIN_HOME;
	    	logger.info("1. Authentication Succcess For defaultTargetURL: " + defaultTargetURL);
	    }
	    else if(roles.contains(Constants.ROLE_USER)) {
	    	defaultTargetURL=Constants.ROLE_USER_HOME;
	    	logger.info("2. Authentication Succcess For defaultTargetURL: " + defaultTargetURL);
	    }
	    //logger.info("Authentication Succcess For defaultTargetURL: " + defaultTargetURL);
	    return defaultTargetURL;
	}
}
