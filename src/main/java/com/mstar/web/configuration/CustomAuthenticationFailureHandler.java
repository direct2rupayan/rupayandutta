package com.mstar.web.configuration;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;


public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {
	  @Override
	    public void onAuthenticationFailure(
	      HttpServletRequest request,
	      HttpServletResponse response,
	      AuthenticationException exception) 
	      throws IOException, ServletException {
	  
	        response.setStatus(HttpStatus.UNAUTHORIZED.value());
	        Map<String, Object> data = new HashMap<>();
	        data.put(
	          "timestamp", 
	          Calendar.getInstance().getTime());
	        data.put(
	          "exception", 
	          exception.getMessage());
	 
	        response.sendRedirect("/login");
	    }
}
