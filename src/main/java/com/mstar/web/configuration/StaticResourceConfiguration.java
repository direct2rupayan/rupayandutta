package com.mstar.web.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
@Configuration
public class StaticResourceConfiguration extends WebMvcConfigurerAdapter  {
	
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(
                "/images/**",
                "/styles/**",
                "/scripts/**",
                "/download/**",
                "/fonts/**",
                "/js/**",
                "/vendor-script/**")
                .addResourceLocations(
                        "classpath:/static/assets/images/",
                        "classpath:/static/assets/styles/",
                        "classpath:/static/assets/scripts/",
                        "classpath:/static/assets/download/",
                        "classpath:/static/assets/fonts/",
                        "classpath:/static/assets/js/",
                        "classpath:/static/assets/vendor-script/");
    }
	

}

