package com.mstar.web.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.mstar.web.account.service.UserDetailsServiceAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, proxyTargetClass = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	UserDetailsService userDetailsServiceAdapter;
	
	@Autowired
	DataSource dataSource;
	
	@Autowired
	private SimpleAuthenticationSuccessHandler successHandler;
	
	   @Bean
	    public BCryptPasswordEncoder passwordEncoder() {
	        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
	        return bCryptPasswordEncoder;
	    }
	     
	     
	    @Autowired
	    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception { 
	        auth.userDetailsService(userDetailsServiceAdapter).passwordEncoder(passwordEncoder());     
	    }
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {	
		http.csrf().disable();
		http
		.authorizeRequests()
		.antMatchers("/resources/**","/static/assets/**","/assets/fonts/**","/assets/webjars/**","/assets/scripts/**","/assets/download/**","/assets/styles/**","/assets/images/**","/assets/js/**","/assets/vendor-script/**").permitAll()
		.antMatchers("/","/index.html","/login_failed.html","/login.html","/login","/student/get","/student/create","/getObjectives").permitAll()
		.anyRequest().authenticated();
		 http.authorizeRequests().and().formLogin().failureForwardUrl("/login_failure_handler")//
         .loginProcessingUrl("/j_spring_security_check") // Submit URL
         .usernameParameter("username")//
         .passwordParameter("password")
         .successHandler(successHandler)
         .and().logout().logoutSuccessUrl("/").invalidateHttpSession(true)
         .deleteCookies("JSESSIONID")
         
/*         .failureUrl("/login")
         .and().logout().logoutUrl("/login")*/
         //.deleteCookies("my-remember-me-cookie")
         .permitAll();
/*         .and()
      .rememberMe().rememberMeParameter("")
       //.key("my-secure-key")
       .rememberMeCookieName("my-remember-me-cookie")
       .tokenRepository(persistentTokenRepository())
       .tokenValiditySeconds(24 * 60 * 60)
       .and()
     .exceptionHandling()
       ;*/
		
	}
	
	     PersistentTokenRepository persistentTokenRepository(){
	     JdbcTokenRepositoryImpl tokenRepositoryImpl = new JdbcTokenRepositoryImpl();
	     tokenRepositoryImpl.setDataSource(dataSource);
	     return tokenRepositoryImpl;
	    }
	     
	     
	     @Bean
	     public AuthenticationFailureHandler customAuthenticationFailureHandler() {
	         return new CustomAuthenticationFailureHandler();
	     }

}
