package com.mstar.web.GlobalConstants;

public class Constants {
public static String ROLE_ADMIN="ROLE_ADMIN";
public static String ROLE_USER="ROLE_USER";
public static String ROLE_GUEST="ROLE_GUEST";
public static String ROLE_INSTRUCTOR="ROLE_INSTRUCTOR";

public static String ROLE_ADMIN_HOME="/admin";
public static String ROLE_USER_HOME="/student";
public static String ROLE_GUEST_HOME="/";
public static String ROLE_INSTRUCTOR_HOME="/";

public static long Overview_Objectives= 1;
public static long Overview_Recall_Concepts=2;
public static long Overview_Scenario_Question=3;


public static long Detailed_Study_Concepts= 4;
public static long Detailed_Study_Practical =5;
public static long Detailed_Study_Case_Study=6;


public static long Add_Ons_Additional_Information= 7;
public static long Add_Ons_Podcasts_Audio_Lessons =8;
public static long Add_Ons_Your_Project=9;
public static long Add_Ons_Research_Experiment= 10;


public static long Review_Summary= 11;
public static long Review_Mind_Map =12;
public static long  Review_Podcast=13;
public static long Review_Notes= 14;

public static long QA_Objective= 15;
public static long QA_Subjective =16;
public static long QA_Analysis_Answer=17;
public static long QA_NCERT= 18;


}
