package com.mstar.web.account.model;

public class MemberDTO {
	
	private Long userId;
	private String login;
	private String encrytedPassword;
	private Integer active;
	
	private Long memberDetailId;
	private Long memberId;
	
	private Long memberGroupID;
	private Long groupID;
	
	public MemberDTO(Long userId, String login, String encrytedPassword, Integer active, Long memberDetailId,
			Long memberId, Long memberGroupID, Long groupID) {
		super();
		this.userId = userId;
		this.login = login;
		this.encrytedPassword = encrytedPassword;
		this.active = active;
		this.memberDetailId = memberDetailId;
		this.memberId = memberId;
		this.memberGroupID = memberGroupID;
		this.groupID = groupID;
	}
	
	public MemberDTO() {
		super();
	}
	
	public Long getUserId() {
		return userId;
	}
	
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getEncrytedPassword() {
		return encrytedPassword;
	}
	public void setEncrytedPassword(String encrytedPassword) {
		this.encrytedPassword = encrytedPassword;
	}
	public Integer getActive() {
		return active;
	}
	public void setActive(Integer active) {
		this.active = active;
	}
	public Long getMemberDetailId() {
		return memberDetailId;
	}
	public void setMemberDetailId(Long memberDetailId) {
		this.memberDetailId = memberDetailId;
	}
	public Long getMemberId() {
		return memberId;
	}
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	public Long getMemberGroupID() {
		return memberGroupID;
	}
	public void setMemberGroupID(Long memberGroupID) {
		this.memberGroupID = memberGroupID;
	}
	
	public Long getGroupID() {
		return groupID;
	}
	public void setGroupID(Long groupID) {
		this.groupID = groupID;
	}
	 

}
