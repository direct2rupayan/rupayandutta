package com.mstar.web.account.model;

public class EventsDTO {
	
	private String id;
	private String title;
	private String start;
	private String end;
	private String details;
	private String classNames;
	private String grade;
	private String subject;
	private String chapter;
	private String remarks;
	
	
	public EventsDTO(String id, String title, String start, String end, String details, String classNames, String grade,
			String subject, String chapter, String remarks) {
		super();
		this.id = id;
		this.title = title;
		this.start = start;
		this.end = end;
		this.details = details;
		this.classNames = classNames;
		this.grade = grade;
		this.subject = subject;
		this.chapter = chapter;
		this.remarks = remarks;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public EventsDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getClassNames() {
		return classNames;
	}
	public void setClassNames(String classNames) {
		this.classNames = classNames;
	}
	
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getChapter() {
		return chapter;
	}
	public void setChapter(String chapter) {
		this.chapter = chapter;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "EventsDTO [id=" + id + ", title=" + title + ", start=" + start + ", end=" + end + ", details=" + details
				+ ", classNames=" + classNames + ", grade=" + grade + ", subject=" + subject + ", chapter=" + chapter
				+ ", remarks=" + remarks + "]";
	}
	
	
	
}
