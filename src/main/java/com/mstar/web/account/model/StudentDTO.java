package com.mstar.web.account.model;

import java.io.Serializable;

import javax.persistence.Column;

import org.springframework.stereotype.Component;
@Component
public class StudentDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	public StudentDTO() {
		super();
	}

	public StudentDTO(Long sId, String enrollmentNumber, String name, String dtOfBirth, String grade, String section,
			String bloodGr, String nationality, String religion, String category, String email, String phone,
			String currentStreetAddress, String currentPostOffice, String currentPoliceStation, String currentPIN,
			String permStreetAddress, String permPostOffice, String permPoliceStation, String permPIN, Long pId,
			String pName, String pRelation, String pOccupation, String pEmail, String pPhone, String gender , Integer status,String aadhar) {
		super();
		this.sId = sId;
		this.enrollmentNumber = enrollmentNumber;
		this.name = name;
		this.dtOfBirth = dtOfBirth;
		this.grade = grade;
		this.section = section;
		this.bloodGr = bloodGr;
		this.nationality = nationality;
		this.religion = religion;
		this.category = category;
		this.email = email;
		this.phone = phone;
		this.currentStreetAddress = currentStreetAddress;
		this.currentPostOffice = currentPostOffice;
		this.currentPoliceStation = currentPoliceStation;
		this.currentPIN = currentPIN;
		this.permStreetAddress = permStreetAddress;
		this.permPostOffice = permPostOffice;
		this.permPoliceStation = permPoliceStation;
		this.permPIN = permPIN;
		this.pId = pId;
		this.pName = pName;
		this.pRelation = pRelation;
		this.pOccupation = pOccupation;
		this.pEmail = pEmail;
		this.pPhone = pPhone;
		this.gender= gender;
		this.status=status;
		this.setAadhar(aadhar);
	}



	private Long sId;
	private String enrollmentNumber;
	private String name;
	private String dtOfBirth;
	private String grade;
	private String section;
	private String bloodGr;
	private String nationality;
	private String religion;
	private String category;
	private String email;
	private String phone;
	private String currentStreetAddress;
	private String currentPostOffice;
	private String currentPoliceStation;
	private String currentPIN;
	private String permStreetAddress;
	private String permPostOffice;
	private String permPoliceStation;
	private String permPIN;
	//
	private String gender;
	private Integer status;
	private String sibEnrollmentNum;
	private String sibName;
	private String sibGrade;
	private String sibSection;
	private String aadhar;
	
   /* Parent Details */
	private Long pId;
	private String pName;
	private String pRelation;
	private String pOccupation;
	private String pEmail;
	private String pPhone;
	private long classId;
	public Long getsId() {
		return sId;
	}

	public void setsId(Long sId) {
		this.sId = sId;
	}

	public String getEnrollmentNumber() {
		return enrollmentNumber;
	}

	public void setEnrollmentNumber(String enrollmentNumber) {
		this.enrollmentNumber = enrollmentNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDtOfBirth() {
		return dtOfBirth;
	}

	public void setDtOfBirth(String dtOfBirth) {
		this.dtOfBirth = dtOfBirth;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getBloodGr() {
		return bloodGr;
	}

	public void setBloodGr(String bloodGr) {
		this.bloodGr = bloodGr;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCurrentStreetAddress() {
		return currentStreetAddress;
	}

	public void setCurrentStreetAddress(String currentStreetAddress) {
		this.currentStreetAddress = currentStreetAddress;
	}

	public String getCurrentPostOffice() {
		return currentPostOffice;
	}

	public void setCurrentPostOffice(String currentPostOffice) {
		this.currentPostOffice = currentPostOffice;
	}

	public String getCurrentPoliceStation() {
		return currentPoliceStation;
	}

	public void setCurrentPoliceStation(String currentPoliceStation) {
		this.currentPoliceStation = currentPoliceStation;
	}

	public String getCurrentPIN() {
		return currentPIN;
	}

	public void setCurrentPIN(String currentPIN) {
		this.currentPIN = currentPIN;
	}

	public String getPermStreetAddress() {
		return permStreetAddress;
	}

	public void setPermStreetAddress(String permStreetAddress) {
		this.permStreetAddress = permStreetAddress;
	}

	public String getPermPostOffice() {
		return permPostOffice;
	}

	public void setPermPostOffice(String permPostOffice) {
		this.permPostOffice = permPostOffice;
	}

	public String getPermPoliceStation() {
		return permPoliceStation;
	}

	public void setPermPoliceStation(String permPoliceStation) {
		this.permPoliceStation = permPoliceStation;
	}

	public String getPermPIN() {
		return permPIN;
	}

	public void setPermPIN(String permPIN) {
		this.permPIN = permPIN;
	}

	public Long getpId() {
		return pId;
	}

	public void setpId(Long pId) {
		this.pId = pId;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public String getpRelation() {
		return pRelation;
	}

	public void setpRelation(String pRelation) {
		this.pRelation = pRelation;
	}

	public String getpOccupation() {
		return pOccupation;
	}

	public void setpOccupation(String pOccupation) {
		this.pOccupation = pOccupation;
	}

	public String getpEmail() {
		return pEmail;
	}

	public void setpEmail(String pEmail) {
		this.pEmail = pEmail;
	}

	public String getpPhone() {
		return pPhone;
	}

	public void setpPhone(String pPhone) {
		this.pPhone = pPhone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "StudentDTO [sId=" + sId + ", enrollmentNumber=" + enrollmentNumber + ", name=" + name + ", dtOfBirth="
				+ dtOfBirth + ", grade=" + grade + ", section=" + section + ", bloodGr=" + bloodGr + ", nationality="
				+ nationality + ", religion=" + religion + ", category=" + category + ", email=" + email + ", phone="
				+ phone + ", currentStreetAddress=" + currentStreetAddress + ", currentPostOffice=" + currentPostOffice
				+ ", currentPoliceStation=" + currentPoliceStation + ", currentPIN=" + currentPIN
				+ ", permStreetAddress=" + permStreetAddress + ", permPostOffice=" + permPostOffice
				+ ", permPoliceStation=" + permPoliceStation + ", permPIN=" + permPIN + ", pId=" + pId + ", pName="
				+ pName + ", pRelation=" + pRelation + ", pOccupation=" + pOccupation + ", pEmail=" + pEmail
				+ ", pPhone=" + pPhone + ", getsId()=" + getsId() + ", getEnrollmentNumber()=" + getEnrollmentNumber()
				+ ", getName()=" + getName() + ", getDtOfBirth()=" + getDtOfBirth() + ", getGrade()=" + getGrade()
				+ ", getSection()=" + getSection() + ", getBloodGr()=" + getBloodGr() + ", getNationality()="
				+ getNationality() + ", getReligion()=" + getReligion() + ", getCategory()=" + getCategory()
				+ ", getEmail()=" + getEmail() + ", getPhone()=" + getPhone() + ", getCurrentStreetAddress()="
				+ getCurrentStreetAddress() + ", getCurrentPostOffice()=" + getCurrentPostOffice()
				+ ", getCurrentPoliceStation()=" + getCurrentPoliceStation() + ", getCurrentPIN()=" + getCurrentPIN()
				+ ", getPermStreetAddress()=" + getPermStreetAddress() + ", getPermPostOffice()=" + getPermPostOffice()
				+ ", getPermPoliceStation()=" + getPermPoliceStation() + ", getPermPIN()=" + getPermPIN()
				+ ", getpId()=" + getpId() + ", getpName()=" + getpName() + ", getpRelation()=" + getpRelation()
				+ ", getpOccupation()=" + getpOccupation() + ", getpEmail()=" + getpEmail() + ", getpPhone()="
				+ getpPhone() + ", getGender()=" + getGender() + ", getStatus()=" + getStatus() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

	public String getSibEnrollmentNum() {
		return sibEnrollmentNum;
	}

	public void setSibEnrollmentNum(String sibEnrollmentNum) {
		this.sibEnrollmentNum = sibEnrollmentNum;
	}

	public String getSibName() {
		return sibName;
	}

	public void setSibName(String sibName) {
		this.sibName = sibName;
	}

	public String getSibGrade() {
		return sibGrade;
	}

	public void setSibGrade(String sibGrade) {
		this.sibGrade = sibGrade;
	}

	public String getSibSection() {
		return sibSection;
	}

	public void setSibSection(String sibSection) {
		this.sibSection = sibSection;
	}

	public String getAadhar() {
		return aadhar;
	}

	public void setAadhar(String aadhar) {
		this.aadhar = aadhar;
	}

	public long getClassId() {
		return classId;
	}

	public void setClassId(long classId) {
		this.classId = classId;
	}

}
