package com.mstar.web.account.model;

public class MasterDTO {
	
	private String value;
	private String desc;
	public MasterDTO(String value, String desc) {
		super();
		this.value = value;
		this.desc = desc;
	}
	public MasterDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	@Override
	public String toString() {
		return "MasterDTO [value=" + value + ", desc=" + desc + "]";
	}

}
