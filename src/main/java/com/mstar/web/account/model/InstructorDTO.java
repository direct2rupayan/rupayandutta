package com.mstar.web.account.model;

public class InstructorDTO {
	
	private Long insId;
	private String employmentNumber;
	private String name;
	private String dtOfBirth;
	private String bloodGr;	 
	private String nationality;
	private String religion;
	private String category;
	private String email;
	private String phone;
	private String currentStreetAddress;
	private String currentPostOffice;
	private String currentPoliceStation;
	private String currentPIN;
	private String permStreetAddress;
	private String permPostOffice;
	private String permPoliceStation;
	private String permPIN;
	private String primarySubjects;
	private String secondarySubjects;
	private String teachingInGrades;
	private String gender;
	private Integer status;
	private String aadhar;
	
	public Long getInsId() {
		return insId;
	}
	public void setInsId(Long insId) {
		this.insId = insId;
	}
	public String getEmploymentNumber() {
		return employmentNumber;
	}
	public void setEmploymentNumber(String employmentNumber) {
		this.employmentNumber = employmentNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDtOfBirth() {
		return dtOfBirth;
	}
	public void setDtOfBirth(String dtOfBirth) {
		this.dtOfBirth = dtOfBirth;
	}
	public String getBloodGr() {
		return bloodGr;
	}
	public void setBloodGr(String bloodGr) {
		this.bloodGr = bloodGr;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCurrentStreetAddress() {
		return currentStreetAddress;
	}
	public void setCurrentStreetAddress(String currentStreetAddress) {
		this.currentStreetAddress = currentStreetAddress;
	}
	public String getCurrentPostOffice() {
		return currentPostOffice;
	}
	public void setCurrentPostOffice(String currentPostOffice) {
		this.currentPostOffice = currentPostOffice;
	}
	public String getCurrentPoliceStation() {
		return currentPoliceStation;
	}
	public void setCurrentPoliceStation(String currentPoliceStation) {
		this.currentPoliceStation = currentPoliceStation;
	}
	public String getCurrentPIN() {
		return currentPIN;
	}
	public void setCurrentPIN(String currentPIN) {
		this.currentPIN = currentPIN;
	}
	public String getPermStreetAddress() {
		return permStreetAddress;
	}
	public void setPermStreetAddress(String permStreetAddress) {
		this.permStreetAddress = permStreetAddress;
	}
	public String getPermPostOffice() {
		return permPostOffice;
	}
	public void setPermPostOffice(String permPostOffice) {
		this.permPostOffice = permPostOffice;
	}
	public String getPermPoliceStation() {
		return permPoliceStation;
	}
	public void setPermPoliceStation(String permPoliceStation) {
		this.permPoliceStation = permPoliceStation;
	}
	public String getPermPIN() {
		return permPIN;
	}
	public void setPermPIN(String permPIN) {
		this.permPIN = permPIN;
	}
	public String getPrimarySubjects() {
		return primarySubjects;
	}
	public void setPrimarySubjects(String primarySubjects) {
		this.primarySubjects = primarySubjects;
	}
	public String getSecondarySubjects() {
		return secondarySubjects;
	}
	public void setSecondarySubjects(String secondarySubjects) {
		this.secondarySubjects = secondarySubjects;
	}
	public String getTeachingInGrades() {
		return teachingInGrades;
	}
	public void setTeachingInGrades(String teachingInGrades) {
		this.teachingInGrades = teachingInGrades;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "InstructorDTO [insId=" + insId + ", employmentNumber=" + employmentNumber + ", name=" + name
				+ ", dtOfBirth=" + dtOfBirth + ", bloodGr=" + bloodGr + ", nationality=" + nationality + ", religion="
				+ religion + ", category=" + category + ", email=" + email + ", phone=" + phone
				+ ", currentStreetAddress=" + currentStreetAddress + ", currentPostOffice=" + currentPostOffice
				+ ", currentPoliceStation=" + currentPoliceStation + ", currentPIN=" + currentPIN
				+ ", permStreetAddress=" + permStreetAddress + ", permPostOffice=" + permPostOffice
				+ ", permPoliceStation=" + permPoliceStation + ", permPIN=" + permPIN + ", primarySubjects="
				+ primarySubjects + ", secondarySubjects=" + secondarySubjects + ", teachingInGrades="
				+ teachingInGrades + ", gender=" + gender + ", status=" + status + "]";
	}
	
	public InstructorDTO() {
		super();
	}
	
	public InstructorDTO(Long insId, String employmentNumber, String name, String dtOfBirth, String bloodGr,
			String nationality, String religion, String category, String email, String phone,
			String currentStreetAddress, String currentPostOffice, String currentPoliceStation, String currentPIN,
			String permStreetAddress, String permPostOffice, String permPoliceStation, String permPIN,
			String primarySubjects, String secondarySubjects, String teachingInGrades, String gender, Integer status,String aadhar) {
		super();
		this.insId = insId;
		this.employmentNumber = employmentNumber;
		this.name = name;
		this.dtOfBirth = dtOfBirth;
		this.bloodGr = bloodGr;
		this.nationality = nationality;
		this.religion = religion;
		this.category = category;
		this.email = email;
		this.phone = phone;
		this.currentStreetAddress = currentStreetAddress;
		this.currentPostOffice = currentPostOffice;
		this.currentPoliceStation = currentPoliceStation;
		this.currentPIN = currentPIN;
		this.permStreetAddress = permStreetAddress;
		this.permPostOffice = permPostOffice;
		this.permPoliceStation = permPoliceStation;
		this.permPIN = permPIN;
		this.primarySubjects = primarySubjects;
		this.secondarySubjects = secondarySubjects;
		this.teachingInGrades = teachingInGrades;
		this.gender = gender;
		this.status = status;
		this.aadhar=aadhar;
	}
	public String getAadhar() {
		return aadhar;
	}
	public void setAadhar(String aadhar) {
		this.aadhar = aadhar;
	}


}
