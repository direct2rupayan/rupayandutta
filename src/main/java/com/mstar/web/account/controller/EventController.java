package com.mstar.web.account.controller;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.mstar.web.account.model.EventsDTO;
import com.mstar.web.account.model.StudentDTO;
import com.mstar.web.account.service.IEventsService;

@RestController
@RequestMapping(value={"/events"})
public class EventController {
	
	private static final Logger LOG = LoggerFactory.getLogger(EventController.class);
	/*@GetMapping("/api/events")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    Iterable<Events> events(@RequestParam("start") @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime start, @RequestParam("end") @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime end) {

        return null;
    }*/
	
	@Autowired
	private IEventsService eventsService;
	
	@GetMapping("/get/{schTyp}")
	   public ResponseEntity<List<EventsDTO>> getAllEvents(Principal principal,@PathVariable("schTyp")String schTyp) {
		String userName = principal.getName();
    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
        //List<EventsDTO> events=eventsService.getEvents();
    	List<EventsDTO> events=eventsService.getEventsforMember(loginedUser.getUsername(),schTyp);
        ResponseEntity<List<EventsDTO>> re=null;
        if(events!=null && events.size()>0){
        	re =new ResponseEntity<List<EventsDTO>>(events, HttpStatus.OK);
        }else{
        	re = new ResponseEntity<List<EventsDTO>>(HttpStatus.NOT_FOUND);
        }
        return re;
    } 
	
	 @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<EventsDTO> createnewEvent(@RequestBody Map<String, Object> fields) {
	    	
	    	LOG.info("INSIDE FOR CREATING EVENTS DETAILS :");
	    	String schType=(String)fields.get("schType");
	    	String schTitle=(String)fields.get("schTitle");
	    	String schDate=(String)fields.get("schDate");
	    	String schStart=(String)fields.get("schStart");
	    	String schEnd=(String)fields.get("schEnd");
	    	String schGrade=(String)fields.get("schGrade");
	    	String schSubject=(String)fields.get("schSubject");
	    	String schRemarks=(String)fields.get("schRemarks");
	    	String schChapter=(String)fields.get("schChapter");
	    	
	    	String loginName=(String)fields.get("loginName");
	    	String eventId=(String)fields.get("eventID");
	    	
	    	
	    	LOG.info(schType+" "+schTitle+" "+schDate+" "+schStart+" "+schEnd+" "+schGrade+" "+schSubject+" "+loginName+" "+eventId);
	    	
	    	int created=eventsService.createEvent(schType,schTitle,schDate,schStart,schEnd,schGrade,schSubject,schRemarks.substring(0, 30),schChapter,loginName);
	    	
	    	if(created>0)
	        return new ResponseEntity<EventsDTO>(HttpStatus.CREATED);
	    	else
	    		return new ResponseEntity<EventsDTO>(HttpStatus.EXPECTATION_FAILED);
	    }
	 
	 @PutMapping(value = "/modify", produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<EventsDTO> updateEvent(@RequestBody Map<String, Object> fields) {
	    	
	    	LOG.info("INSIDE FOR Modify EVENTS DETAILS :");
	    	String schType=(String)fields.get("schType");
	    	String schTitle=(String)fields.get("schTitle");
	    	String schDate=(String)fields.get("schDate");
	    	String schStart=(String)fields.get("schStart");
	    	String schEnd=(String)fields.get("schEnd");
	    	String schGrade=(String)fields.get("schGrade");
	    	String schSubject=(String)fields.get("schSubject");
	    	String schRemarks=(String)fields.get("schRemarks");
	    	String schChapter=(String)fields.get("schChapter");
	    	
	    	String loginName=(String)fields.get("loginName");
	    	Long eventId=0L;
	    	String even=(String)fields.get("eventID");
	    	if(even!=null && !even.isEmpty() && !"0".equals(even)){
	    		eventId=Long.parseLong(even);
	    	}else{
	    		return new ResponseEntity<EventsDTO>(HttpStatus.EXPECTATION_FAILED);
	    	}
	    	
	    	
	    	LOG.info(schType+" "+schTitle+" "+schDate+" "+schStart+" "+schEnd+" "+schGrade+" "+schSubject+" "+loginName+" "+eventId);
	    	
	    	int modified=eventsService.modifyEvent(schType,schTitle,schDate,schStart,schEnd,schGrade,schSubject,schRemarks.substring(0, 30),schChapter,loginName,eventId);
	    	if(modified>0)
	        return new ResponseEntity<EventsDTO>(HttpStatus.OK);
	    	else
	    		return new ResponseEntity<EventsDTO>(HttpStatus.EXPECTATION_FAILED);
	    }
	 
	 @DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<EventsDTO> deleteEvent(@RequestBody Map<String, Object> fields) {
	    	
	    	LOG.info("INSIDE FOR Delete EVENTS DETAILS :");
	    	
	    	Long eventId=0L;
	    	String even=(String)fields.get("eventID");
	    	if(even!=null && !even.isEmpty() && !"0".equals(even)){
	    		eventId=Long.parseLong(even);
	    	}else{
	    		return new ResponseEntity<EventsDTO>(HttpStatus.EXPECTATION_FAILED);
	    	}
	    	
	    	LOG.info("For Delete EventID"+eventId);
	    	
	    	int deleted=eventsService.deleteEvent(eventId);
	    	if(deleted>0)
	        return new ResponseEntity<EventsDTO>(HttpStatus.OK);
	    	else
	    		return new ResponseEntity<EventsDTO>(HttpStatus.EXPECTATION_FAILED);
	    }
}
