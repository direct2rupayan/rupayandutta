package com.mstar.web.account.controller;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.mstar.web.account.entity.Parent;
import com.mstar.web.account.entity.Student;
import com.mstar.web.account.model.MemberDTO;
import com.mstar.web.account.model.StudentDTO;
import com.mstar.web.account.service.IMemberService;
import com.mstar.web.account.service.IStudentService;
import com.mstar.web.account.service.MemberService;

@RestController
@RequestMapping(value={"/student"})
public class StudentController {
	
	private static final Logger LOG = LoggerFactory.getLogger(StudentController.class);
	
	@Autowired
	private IStudentService studentService;
	
	@Autowired
	private IMemberService memberService;
	
    @GetMapping(value="/get", headers="Accept=application/json")
    public List<StudentDTO> getAllUser() {
        List<StudentDTO> students=studentService.getStudent();
        return students;
    }
    
    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createUser(@RequestBody StudentDTO student, UriComponentsBuilder ucBuilder){
    	LOG.info("student.getStatus "+student.getStatus());
    	LOG.info("INSIDE FOR CREATING STUDENT");
         studentService.addStudent(student);
         MemberDTO mdto=new MemberDTO();
         mdto.setLogin(student.getEmail());
         mdto.setActive(1);
         mdto.setEncrytedPassword("123#");
         mdto.setMemberGroupID(2L);
         memberService.addMember(mdto);
         HttpHeaders headers = new HttpHeaders();
        //headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StudentDTO> getUserById(@PathVariable("id") long id) {
        StudentDTO studentDTO = studentService.findById(id);
        LOG.info("Student Retreived: "+studentDTO.toString());
        if (studentDTO == null) {
            return new ResponseEntity<StudentDTO>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<StudentDTO>(studentDTO, HttpStatus.OK);
    }
    
    @PutMapping(value="/{id}",headers="Accept=application/json")
    public ResponseEntity<Void> updateUser(@RequestBody StudentDTO student, @PathVariable("id") long id, UriComponentsBuilder ucBuilder){
    	LOG.info("INSIDE FOR UPDATING STUDENT : "+student.getGender());
         studentService.update(student, id);
         HttpHeaders headers = new HttpHeaders();
        //headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}", headers ="Accept=application/json")
    public ResponseEntity<StudentDTO> deleteUser(@PathVariable("id") long id){
    	StudentDTO studentDTO = studentService.findById(id);
        if (studentDTO == null) {
            return new ResponseEntity<StudentDTO>(HttpStatus.NOT_FOUND);
        }
        studentService.delete(id);
        return new ResponseEntity<StudentDTO>(HttpStatus.OK);
    }
    
    @PatchMapping(value="/{id}", headers ="Accept=application/json")
    public ResponseEntity<Void> updateUserStatus(@PathVariable("id") long id,@RequestBody Map<String, Object> fields){
    	
    	String status= (String)fields.get("status");
    	LOG.info("INSIDE FOR UPDATING STUDENT STATUS :"+id+" Status :"+status);
    	
    	int stat=0;
    	if("INACTIVE".equalsIgnoreCase(status)){
    		stat=1;
    	}
    	int result=studentService.updateStatus(id,stat);
        if (result == 0) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }else
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    
    @PostMapping(value = "/siblinginfo", produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StudentDTO> getSiblingDetails(@RequestBody Map<String, Object> fields) {
    	
    	LOG.info("INSIDE FOR GETTING SIBLING DETAILS :");
    	String name=(String)fields.get("sibname");
    	String grade=(String)fields.get("sibgrade");
    	
        StudentDTO studentDTO = studentService.findSiblingDetails(name,grade);
        LOG.info("Student Retreived: "+studentDTO);
        if (studentDTO == null) {
            return new ResponseEntity<StudentDTO>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<StudentDTO>(studentDTO, HttpStatus.OK);
    }

}
