package com.mstar.web.account.controller;

import java.security.Principal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mstar.web.GlobalConstants.Constants;
import com.mstar.web.content.entity.Chapter;
import com.mstar.web.content.entity.Content;
import com.mstar.web.content.service.IChapterService;
import com.mstar.web.content.service.IContentService;
import com.mstar.web.content.service.ISubjectService;
import com.mstar.web.utils.WebUtils;

@Controller
public class StudentNavigationController {
/*	 @RequestMapping(value="/student" ,method=RequestMethod.GET)
	    public String schoolDashboard(Model model,Principal principal) {
	    	String userName = principal.getName();
	        User loginedUser = (User) ((Authentication) principal).getPrincipal();
	        String userInfo = WebUtils.toString(loginedUser);
	        model.addAttribute("userInfo", userInfo);
	        return "student-index";
	    }*/
	
	    @Autowired
	    private ISubjectService subjectService;
	    
		@Autowired
		private IChapterService chapterService;
		
		
	    @Autowired
	    private IContentService contentService;
	    

	    
	    @RequestMapping(value="/student-learn" ,method=RequestMethod.GET)
	    public String studentlearn(@RequestParam(name="classId") String classId,Model model,Principal principal) {
	    	String userName = principal.getName();
	    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
	        String userInfo = WebUtils.toString(loginedUser);
	        model.addAttribute("userInfo", userInfo);
	        model.addAttribute("classId", classId);
	        model.addAttribute("ajaxResponseBodyClasses", subjectService.getChapterCountForClass(Long.parseLong(classId)));
	        return "student-learn";
	    }
	    
	    @RequestMapping(value="/student-chapter" ,method=RequestMethod.GET)
	    public String studentChapter(@RequestParam(name="subjectId") String subjectId,@RequestParam(name="classId") String classId,Model model,Principal principal) {
	    	String userName = principal.getName();
	    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
	        String userInfo = WebUtils.toString(loginedUser);
	        model.addAttribute("userInfo", userInfo);
	        model.addAttribute("classId", classId);
	        List<Chapter> lst= chapterService.getChapters(Long.parseLong(classId), Long.parseLong(subjectId));
	        model.addAttribute("chapters", chapterService.getChapters(Long.parseLong(classId), Long.parseLong(subjectId)));
	        return "student-chapter";
	    }
	    
	    @RequestMapping(value="/student-chapter-details" ,method=RequestMethod.GET)
	    public String studentChapterDetails(@RequestParam(name="classId") String classId,@RequestParam(name="subjectId") String subjectId,@RequestParam(name="chapterId") String chapterId,Model model,Principal principal) {
	    	String userName = principal.getName();
	    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
	        String userInfo = WebUtils.toString(loginedUser);
	        model.addAttribute("userInfo", userInfo);
	        model.addAttribute("classId", classId);
	        model.addAttribute("subjectId", subjectId);
	        model.addAttribute("chapterId", classId);
	        Map<Long, List<Content>> categoryContents = contentService.getContents(Long.parseLong(classId), Long.parseLong(subjectId), Long.parseLong(chapterId));
	        model.addAttribute("Overview_Objectives", categoryContents.get(Constants.Overview_Objectives));
	        model.addAttribute("Overview_Recall_Concepts", categoryContents.get(Constants.Overview_Recall_Concepts));
	        model.addAttribute("Overview_Scenario_Question", categoryContents.get(Constants.Overview_Scenario_Question));
	        return "student-chapter-details";
	    }
	    
	    @RequestMapping(value="/student-live-session" ,method=RequestMethod.GET)
	    public String studentLiveSession(@RequestParam(name="classId") String classId,Model model,Principal principal) {
	    	String userName = principal.getName();
	    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
	        String userInfo = WebUtils.toString(loginedUser);
	        model.addAttribute("userInfo", userInfo);
	        model.addAttribute("classId",classId);
	        Map<String, List<Content>> liveSessionContents = contentService.getContents(Long.parseLong(classId),"LS");
	        model.addAttribute("liveSessionContents", liveSessionContents);
	        return "student-live-session";
	    }
	    
}
