package com.mstar.web.account.controller;

import java.security.Principal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.mstar.web.GlobalConstants.Constants;
import com.mstar.web.content.entity.Chapter;
import com.mstar.web.content.entity.Content;
import com.mstar.web.content.service.IChapterService;
import com.mstar.web.content.service.IContentService;
import com.mstar.web.content.service.ISubjectService;
import com.mstar.web.utils.WebUtils;

@Controller
public class AdminUINavigationController {
	
	@Autowired
	private ISubjectService subjectService;
	
	@Autowired
	private IChapterService chapterService;
	
    @Autowired
    private IContentService contentService;
	
    
/*    @RequestMapping(value="/admin" ,method=RequestMethod.GET)
    public String studentDashboard(Model model,Principal principal) {
    	String userName = principal.getName();
    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
        String userInfo = WebUtils.toString(loginedUser);
        model.addAttribute("userInfo", userInfo);
        return "school-index";
    }*/
    
    
    @RequestMapping(value="/school-notice-board" ,method=RequestMethod.GET)
    public String schoolNoticeboard(Model model,Principal principal) {
    	String userName = principal.getName();
    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
        String userInfo = WebUtils.toString(loginedUser);
        model.addAttribute("userInfo", userInfo);
        return "school-notice-board";
    } 
    
    
    @RequestMapping(value="/school-notification" ,method=RequestMethod.GET)
    public String schoolNotification(Model model,Principal principal) {
    	String userName = principal.getName();
    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
        String userInfo = WebUtils.toString(loginedUser);
        model.addAttribute("userInfo", userInfo);
        return "school-notification";
    } 
    
    @RequestMapping(value="/school-upload-content" ,method=RequestMethod.GET)
    public String studentUploadcontent(Model model,Principal principal) {
    	String userName = principal.getName();
    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
        String userInfo = WebUtils.toString(loginedUser);
        model.addAttribute("userInfo", userInfo);
        return "school-upload-content";
    }
    
    @RequestMapping(value="/school-upload-content-chapter-details" ,method=RequestMethod.GET)
    public String schoolUploadcontentChapter(@RequestParam(name="classId") String classId,@RequestParam(name="subjectId") String subjectId,@RequestParam(name="chapterId") String chapterId,Model model,Principal principal) {
    	String userName = principal.getName();
    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
        String userInfo = WebUtils.toString(loginedUser);
        model.addAttribute("userInfo", userInfo);
/*        List<Content> objectiveContents = contentService.getContents(Long.parseLong(classId), Long.parseLong(subjectId), Long.parseLong(chapterId), 1L);
        System.out.println(objectiveContents.size() + "is the Size" );
        model.addAttribute("objectiveContents", objectiveContents);*/
        Map<Long, List<Content>> categoryContents = contentService.getContents(Long.parseLong(classId), Long.parseLong(subjectId), Long.parseLong(chapterId));
        model.addAttribute("Overview_Objectives", categoryContents.get(Constants.Overview_Objectives));
        model.addAttribute("Overview_Recall_Concepts", categoryContents.get(Constants.Overview_Recall_Concepts));
        model.addAttribute("Overview_Scenario_Question", categoryContents.get(Constants.Overview_Scenario_Question));
        
        model.addAttribute("Detailed_Study_Concepts", categoryContents.get(Constants.Detailed_Study_Concepts));
        model.addAttribute("Detailed_Study_Practical", categoryContents.get(Constants.Detailed_Study_Practical));
        model.addAttribute("Detailed_Study_Case_Study", categoryContents.get(Constants.Detailed_Study_Case_Study));
        
        model.addAttribute("Add_Ons_Additional_Information", categoryContents.get(Constants.Add_Ons_Additional_Information));
        model.addAttribute("Add_Ons_Podcasts_Audio_Lessons", categoryContents.get(Constants.Add_Ons_Podcasts_Audio_Lessons));
        model.addAttribute("Add_Ons_Your_Project", categoryContents.get(Constants.Add_Ons_Your_Project));
        model.addAttribute("Add_Ons_Research_Experiment", categoryContents.get(Constants.Add_Ons_Research_Experiment));
        
        model.addAttribute("Review_Summary", categoryContents.get(Constants.Review_Summary));
        model.addAttribute("Review_Mind_Map", categoryContents.get(Constants.Review_Mind_Map));
        model.addAttribute("Review_Podcast", categoryContents.get(Constants.Review_Podcast));
        model.addAttribute("Review_Notes", categoryContents.get(Constants.Review_Notes));
        
        
        model.addAttribute("QA_Objective", categoryContents.get(Constants.QA_Objective));
        model.addAttribute("QA_Subjective", categoryContents.get(Constants.QA_Subjective));
        model.addAttribute("QA_Analysis_Answer", categoryContents.get(Constants.QA_Analysis_Answer));
        model.addAttribute("QA_NCERT", categoryContents.get(Constants.QA_NCERT));

        
        
        return "school-upload-content-chapter-details";
    }
    
    @RequestMapping(value="/school-upload-content-chapter" ,method=RequestMethod.GET)
    public String studentUploadcontentChapter(@RequestParam(name="subjectId") String subjectId,@RequestParam(name="classId") String classId,Model model,Principal principal) {
    	String userName = principal.getName();
    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
        String userInfo = WebUtils.toString(loginedUser);
        model.addAttribute("userInfo", userInfo);
        List<Chapter> lst= chapterService.getChapters(Long.parseLong(classId), Long.parseLong(subjectId));
        model.addAttribute("chapters", chapterService.getChapters(Long.parseLong(classId), Long.parseLong(subjectId)));
        return "school-upload-content-chapter";
    }
    
    @RequestMapping(value="/school-upload-content-subject" ,method=RequestMethod.GET)
    public String studentUploadcontentSubject(@RequestParam(name="classId") String classId,Model model,Principal principal) {
    	String userName = principal.getName();
    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
        String userInfo = WebUtils.toString(loginedUser);
        model.addAttribute("userInfo", userInfo);
        model.addAttribute("ajaxResponseBodyClasses", subjectService.getChapterCountForClass(Long.parseLong(classId)));
        return "school-upload-content-subject";
    }
   
    
    
    @RequestMapping(value="/school-live-session" ,method=RequestMethod.GET)
    public String schoolLiveSession(Model model,Principal principal) {
    	String userName = principal.getName();
    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
        String userInfo = WebUtils.toString(loginedUser);
        model.addAttribute("userInfo", userInfo);
        Map<String, List<Content>> liveSessionContents = contentService.getContents(null,"LS");
        model.addAttribute("liveSessionContents", liveSessionContents);
        return "/school-live-session";
    }
	
}
