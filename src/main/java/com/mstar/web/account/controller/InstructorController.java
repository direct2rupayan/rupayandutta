package com.mstar.web.account.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import com.mstar.web.account.entity.Instructor;
import com.mstar.web.account.model.InstructorDTO;
import com.mstar.web.account.model.MemberDTO;
import com.mstar.web.account.model.StudentDTO;
import com.mstar.web.account.service.IInstructorService;
import com.mstar.web.account.service.IMemberService;
import com.mstar.web.account.service.InstructorService;


@RestController
@RequestMapping(value={"/instructor"})
public class InstructorController {
	
	private static final Logger LOG = LoggerFactory.getLogger(InstructorController.class);
	
	@Autowired
	private IInstructorService instructorService;
	
	@Autowired
	private IMemberService memberService;
	
	 @GetMapping(value="/get", headers="Accept=application/json")
	    public List<InstructorDTO> getAllnstructor() {
	        List<InstructorDTO> instructors=instructorService.getInstructor();
	        return instructors;
	    }
	    
	    @PostMapping(value="/create",headers="Accept=application/json")
	    public ResponseEntity<Void> createUser(@RequestBody Instructor instructor, UriComponentsBuilder ucBuilder){	
	    	LOG.info("Inside create Instructor"+ instructor.toString()+ ","+instructor.getEmploymentNumber());
	    	instructorService.addInstructor(instructor);
	    	 MemberDTO mdto=new MemberDTO();
	         mdto.setLogin(instructor.getEmail());
	         mdto.setActive(1);
	         mdto.setEncrytedPassword("123#");
	         mdto.setMemberGroupID(3L);
	         memberService.addMember(mdto);
	         HttpHeaders headers = new HttpHeaders();
	        //headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
	        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	    }
	    
	    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<InstructorDTO> getInstructorById(@PathVariable("id") long id) {
	    	
	    	LOG.info("Retreive Instructor by ID:"+ id);
	    	InstructorDTO instructor = instructorService.findById(id);
	        if (instructor == null) {
	            return new ResponseEntity<InstructorDTO>(HttpStatus.NOT_FOUND);
	        }
	        LOG.info("Retreived Instructor :"+ instructor.toString());
	        return new ResponseEntity<InstructorDTO>(instructor, HttpStatus.OK);
	    }
	    
	    @DeleteMapping(value="/{id}", headers ="Accept=application/json")
	    public ResponseEntity<Instructor> deleteInstructor(@PathVariable("id") long id){
	    	InstructorDTO instructor = instructorService.findById(id);
	        if (instructor == null) {
	            return new ResponseEntity<Instructor>(HttpStatus.NOT_FOUND);
	        }
	        instructorService.delete(id);
	        return new ResponseEntity<Instructor>(HttpStatus.OK);
	    }
	    
	    @PutMapping(value="/{id}",headers="Accept=application/json")
	    public ResponseEntity<Void> updateInstructor(@RequestBody InstructorDTO student, @PathVariable("id") long id, UriComponentsBuilder ucBuilder){
	    	LOG.info("INSIDE FOR UPDATING STUDENT : "+student.getGender());
	    	instructorService.update(student, id);
	         HttpHeaders headers = new HttpHeaders();
	        //headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
	        return new ResponseEntity<Void>(headers, HttpStatus.OK);
	    }
	    
	    @PatchMapping(value="/{id}", headers ="Accept=application/json")
	    public ResponseEntity<Void> updateUserStatus(@PathVariable("id") long id,@RequestBody Map<String, Object> fields){
	    	
	    	String status= (String)fields.get("status");
	    	LOG.info("INSIDE FOR UPDATING INSTRUCTOR STATUS :"+id+" Status :"+status);
	    	
	    	int stat=0;
	    	if("INACTIVE".equalsIgnoreCase(status)){
	    		stat=1;
	    	}
	    	int result=instructorService.updateStatus(id,stat);
	        if (result == 0) {
	            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	        }else
	        return new ResponseEntity<Void>(HttpStatus.OK);
	    }

}
