package com.mstar.web.account.controller;

import java.security.Principal;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.mstar.web.account.model.MasterDTO;
import com.mstar.web.account.model.StudentDTO;
import com.mstar.web.account.service.IMasterService;
import com.mstar.web.account.service.IStudentService;
import com.mstar.web.utils.WebUtils;

@Controller
public class HomeController {
	
	private static final Logger LOG = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private IMasterService masterSevice;
	
	@Autowired
	private IStudentService studentService;
	

	@Autowired
	private ServletContext servletContext;
	
	@PostConstruct
	private void setMasterData(){
		List<MasterDTO> standards=masterSevice.getAllStandard();
		List<MasterDTO> section=masterSevice.getAllSection();
		List<MasterDTO> bldgrp=masterSevice.getAllCategoryGrp();
		List<MasterDTO> gender=masterSevice.getAllGender();
		List<MasterDTO> subjects=masterSevice.getAllSubjects();
		servletContext.setAttribute("standardLst", standards);
		servletContext.setAttribute("sectionLst", section);
		servletContext.setAttribute("bldGrpLst", bldgrp);
		servletContext.setAttribute("genderLst", gender);
		servletContext.setAttribute("subjectLst", subjects);
	}
	
	@RequestMapping("/")
    public String index(Model model) {
        return "index";
    }
    
	@PostMapping(value="/login_failure_handler")
    public String login(Model model) {
        return "login_failed";
    }

    
	 @RequestMapping(value="/student" ,method=RequestMethod.GET)
	    public String schoolDashboard(Model model,Principal principal) {
	    	String userName = principal.getName();
	        User loginedUser = (User) ((Authentication) principal).getPrincipal();
	        String userInfo = WebUtils.toString(loginedUser);
	        model.addAttribute("userInfo", userInfo);
	        model.addAttribute("stdLst",servletContext.getAttribute("standardLst"));
	        model.addAttribute("secLst",servletContext.getAttribute("sectionLst"));
	        model.addAttribute("bldLst",servletContext.getAttribute("bldGrpLst"));
	        model.addAttribute("genLst",servletContext.getAttribute("genderLst"));
	        StudentDTO dto = studentService.findByLogIn(userName);
	        model.addAttribute("classId",dto.getClassId());
	        return "student-index";
	    }
	    
	    @RequestMapping(value="/admin" ,method=RequestMethod.GET)
	    public String studentDashboard(Model model,Principal principal) {
	    	LOG.info(">> studentDashboard > value Admin ");
	    	String userName = principal.getName();
	    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
	        String userInfo = WebUtils.toString(loginedUser);
	        model.addAttribute("userInfo", userInfo);
	        model.addAttribute("stdLst",servletContext.getAttribute("standardLst"));
	        model.addAttribute("secLst",servletContext.getAttribute("sectionLst"));
	        model.addAttribute("bldLst",servletContext.getAttribute("bldGrpLst"));
	        model.addAttribute("genLst",servletContext.getAttribute("genderLst"));
	        return "school-index";
	    }
	    
	    
	    @RequestMapping(value="/school-schedule-all" ,method=RequestMethod.GET)
	    public String schoolScheduleAll(Model model,Principal principal) {
	    	String userName = principal.getName();
	    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
	        String userInfo = WebUtils.toString(loginedUser);
	        model.addAttribute("userInfo", userInfo);
	        model.addAttribute("userName", loginedUser.getUsername());
	        model.addAttribute("stdLst",servletContext.getAttribute("standardLst"));
	        model.addAttribute("subLst",servletContext.getAttribute("subjectLst"));
	        return "school-schedule-all";
	    }
	    
	    @RequestMapping(value="/student-schedule" ,method=RequestMethod.GET)
	    public String studentSchedule(Model model,Principal principal) {
	    	String userName = principal.getName();
	    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
	        String userInfo = WebUtils.toString(loginedUser);
	        model.addAttribute("userInfo", userInfo);
	        model.addAttribute("userName", loginedUser.getUsername());
	        model.addAttribute("stdLst",servletContext.getAttribute("standardLst"));
	        model.addAttribute("subLst",servletContext.getAttribute("subjectLst"));
	        return "student-schedule";
	    }
	    
	    @RequestMapping(value="/school-schedule-me" ,method=RequestMethod.GET)
	    public String schoolScheduleMe(Model model,Principal principal) {
	    	String userName = principal.getName();
	    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
	        String userInfo = WebUtils.toString(loginedUser);
	        model.addAttribute("userInfo", userInfo);
	        model.addAttribute("userName", loginedUser.getUsername());
	        model.addAttribute("stdLst",servletContext.getAttribute("standardLst"));
	        model.addAttribute("subLst",servletContext.getAttribute("subjectLst"));
	        return "school-schedule-all";
	    }
	    
	    @RequestMapping(value="/student-schedule-me" ,method=RequestMethod.GET)
	    public String studentScheduleMe(Model model,Principal principal) {
	    	String userName = principal.getName();
	    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
	        String userInfo = WebUtils.toString(loginedUser);
	        model.addAttribute("userInfo", userInfo);
	        model.addAttribute("userName", loginedUser.getUsername());
	        model.addAttribute("stdLst",servletContext.getAttribute("standardLst"));
	        model.addAttribute("subLst",servletContext.getAttribute("subjectLst"));
	        return "student-schedule-me";
	    }
    
}
