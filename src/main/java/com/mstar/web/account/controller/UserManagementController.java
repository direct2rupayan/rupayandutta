package com.mstar.web.account.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mstar.web.account.entity.Instructor;
import com.mstar.web.account.model.InstructorDTO;
import com.mstar.web.account.model.StudentDTO;
import com.mstar.web.account.service.IInstructorService;
import com.mstar.web.account.service.IStudentService;
import com.mstar.web.utils.WebUtils;

@Controller
public class UserManagementController {
	
	private static final Logger LOG = LoggerFactory.getLogger(UserManagementController.class);
	
	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	private IStudentService studentService;
	
	@Autowired
	private IInstructorService instructorService;
	
    @RequestMapping(value="/school-student" ,method=RequestMethod.GET)
    public String manageStudent(Model model,Principal principal) {
    	String userName = principal.getName();
    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
        String userInfo = WebUtils.toString(loginedUser);
        //call for student list
        List<StudentDTO> studentList=studentService.getStudent();
        LOG.info("Student List retreived :"+ studentList +" Size of :"+studentList.size());
        //end Call
        model.addAttribute("userInfo", userInfo);
        model.addAttribute("studentLstInfo", studentList);
        model.addAttribute("stdLst",servletContext.getAttribute("standardLst"));
        model.addAttribute("secLst",servletContext.getAttribute("sectionLst"));
        model.addAttribute("bldLst",servletContext.getAttribute("bldGrpLst"));
        model.addAttribute("genLst",servletContext.getAttribute("genderLst"));
        LOG.info("stdLst     "+servletContext.getAttribute("standardLst"));
        return "school-student";
    }  
    
    
    @RequestMapping(value="/school-instructor" ,method=RequestMethod.GET)
    public String manageInstructor(Model model,Principal principal) {
    	String userName = principal.getName();
    	User loginedUser = (User) ((Authentication) principal).getPrincipal();
        String userInfo = WebUtils.toString(loginedUser);
        //call for Instructor list
        List<InstructorDTO> instructorList=instructorService.getInstructor();
        LOG.info("Instructor List retreived :"+ instructorList +" Size of :"+instructorList.size());
        //end Call
        model.addAttribute("userInfo", userInfo);
        model.addAttribute("instructorLstInfo", instructorList);
        model.addAttribute("stdLst",servletContext.getAttribute("standardLst"));
        model.addAttribute("secLst",servletContext.getAttribute("sectionLst"));
        model.addAttribute("bldLst",servletContext.getAttribute("bldGrpLst"));
        model.addAttribute("genLst",servletContext.getAttribute("genderLst"));
        model.addAttribute("subLst",servletContext.getAttribute("subjectLst"));
        LOG.info("stdLst     "+servletContext.getAttribute("standardLst"));
        return "school-instructor";
    }  
}
