package com.mstar.web.account.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//@Table(name = "STANDARD_MST")
@Table(name = "CLASS")
public class Standard {

	@Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 //@Column(name = "STND_ID", nullable = false)
	@Column(name = "CLASS_ID", nullable = false)
	 private Long stdnId;
	
	/* @Column(name = "STND_NAME",length = 50, nullable = false)
	 private String stdnName;*/
	 
	 //@Column(name = "STND_DESC",length = 50, nullable = false)
	@Column(name = "CLASS_NAME",length = 50, nullable = false)
	 private String stdnDesc;

	public Long getStdnId() {
		return stdnId;
	}

	public void setStdnId(Long stdnId) {
		this.stdnId = stdnId;
	}

	/*public String getStdnName() {
		return stdnName;
	}

	public void setStdnName(String stdnName) {
		this.stdnName = stdnName;
	}*/

	public String getStdnDesc() {
		return stdnDesc;
	}

	public void setStdnDesc(String stdnDesc) {
		this.stdnDesc = stdnDesc;
	}
	
}
