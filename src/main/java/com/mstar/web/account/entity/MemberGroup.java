package com.mstar.web.account.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "MEMBER_GROUP")
public class MemberGroup {
	
	@Id
	@GeneratedValue(generator="gen")
	@Column(name = "MEMBER_GROUP_ID", nullable = false)
    @GenericGenerator(name="gen", strategy="foreign",parameters=@Parameter(name="property", value="member"))
	private Long memberGroupID;
	
	
	@Column(name = "GROUP_ID", nullable = false)
	private Long groupID;
	
	public MemberGroup() {
		super();
	}
	
	public MemberGroup(Long groupID){
		super();
		this.groupID = groupID;
	}

	public Long getMemberGroupID() {
		return memberGroupID;
	}
	public void setMemberGroupID(Long memberGroupID) {
		this.memberGroupID = memberGroupID;
	}
	
	public Long getGroupID() {
		return groupID;
	}
	public void setGroupID(Long groupID) {
		this.groupID = groupID;
	}
	
	@OneToOne
	@PrimaryKeyJoinColumn
	private Member member;  
	
	public Member getMember() {
		return member;
	}
	public void setMember(Member member) {
		this.member = member;
	}

}
