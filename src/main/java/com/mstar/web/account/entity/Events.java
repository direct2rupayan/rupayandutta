package com.mstar.web.account.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EVENT_SCHEDULE")
public class Events implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "EVENT_ID", nullable = false)	
	 private Long eventId;
	
	 @Column(name = "EVENT_NAME",length = 250, nullable = false)
	 private String eventName;
	 
	 @Column(name = "EVENT_DESCRIPTION",length = 350, nullable = false)
	 private String eventDescription;
	 
	 @Column(name = "EVENT_START",length = 350, nullable = false)
	 private Date eventStart;
	 
	 @Column(name = "EVENT_END",length = 350, nullable = false)
	 private Date eventEnd;
	 
	 @Column(name = "CREATED_BY",length = 200, nullable = false)
	 private String createdBy;
	 
	 @Column(name = "CREATION_DT",length = 200, nullable = false)
	 private Date creationDt;
	 
	 @Column(name = "EVENT_RECEIVERS",length = 10, nullable = false)
	 private String receivers;
	 
	 @Column(name = "EVENT_GRADE",length = 10, nullable = false)
	 private String eventGrade;
	 
	 @Column(name = "EVENT_SUBJCT",length = 3, nullable = false)
	 private String eventSubjct;
	 
	 @Column(name = "EVENT_CHAPTER",length = 50, nullable = false)
	 private String eventChapter;
	 
	 @Column(name = "EVENT_REMARK",length = 30, nullable = false)
	 private String eventRemark;

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public Date getEventStart() {
		return eventStart;
	}

	public void setEventStart(Date eventStart) {
		this.eventStart = eventStart;
	}

	public Date getEventEnd() {
		return eventEnd;
	}

	public void setEventEnd(Date eventend) {
		this.eventEnd = eventend;
	}

	
	public Events(String eventName, String eventDescription, Date eventStart, Date eventEnd, String createdBy,
			Date creationDt, String receivers, String eventGrade, String eventSubjct, String eventChapter,
			String eventRemark) {
		super();
		this.eventName = eventName;
		this.eventDescription = eventDescription;
		this.eventStart = eventStart;
		this.eventEnd = eventEnd;
		this.createdBy = createdBy;
		this.creationDt = creationDt;
		this.receivers = receivers;
		this.eventGrade = eventGrade;
		this.eventSubjct = eventSubjct;
		this.eventChapter = eventChapter;
		this.eventRemark = eventRemark;
	}

	public Events() {
		super();
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDt() {
		return creationDt;
	}

	public void setCreationDt(Date creationDt) {
		this.creationDt = creationDt;
	}

	public String getReceivers() {
		return receivers;
	}

	public void setReceivers(String receivers) {
		this.receivers = receivers;
	}

	public String getEventGrade() {
		return eventGrade;
	}

	public void setEventGrade(String eventGrade) {
		this.eventGrade = eventGrade;
	}

	public String getEventSubjct() {
		return eventSubjct;
	}

	public void setEventSubjct(String eventSubjct) {
		this.eventSubjct = eventSubjct;
	}

	public String getEventChapter() {
		return eventChapter;
	}

	public void setEventChapter(String eventChapter) {
		this.eventChapter = eventChapter;
	}

	public String getEventRemark() {
		return eventRemark;
	}

	public void setEventRemark(String eventRemark) {
		this.eventRemark = eventRemark;
	}

	@Override
	public String toString() {
		return "Events [eventId=" + eventId + ", eventName=" + eventName + ", eventDescription=" + eventDescription
				+ ", eventStart=" + eventStart + ", eventEnd=" + eventEnd + ", createdBy=" + createdBy + ", creationDt="
				+ creationDt + ", receivers=" + receivers + ", eventGrade=" + eventGrade + ", eventSubjct="
				+ eventSubjct + ", eventChapter=" + eventChapter + ", eventRemark=" + eventRemark + "]";
	}
}
