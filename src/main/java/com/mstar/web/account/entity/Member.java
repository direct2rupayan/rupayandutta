package com.mstar.web.account.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
 
@Entity
@Table(name = "MEMBERS")
public class Member implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "MEMBER_ID", nullable = false)
	private Long userId;
	 
	@Column(name = "LOGIN", length = 250, nullable = false)
    private String login;
	
	@Column(name = "PASSWORD", length = 250, nullable = false)
    private String encrytedPassword;
	
	@Column(name = "ACTIVE",nullable = false)
	private Integer active;

	public Member() {
		super();
	}

    
	public Member(Long userId, String login, String encrytedPassword,Integer active,MemberGroup memberGroup){
		super();
		this.userId = userId;
		this.login = login;
		this.encrytedPassword = encrytedPassword;
		this.memberGroup=memberGroup;
		this.active=active;
	}
	public Long getUserId() {
		return userId;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getEncrytedPassword() {
		return encrytedPassword;
	}
	public void setEncrytedPassword(String encrytedPassword) {
		this.encrytedPassword = encrytedPassword;
	}
	public Integer getActive() {
		return active;
	}
	public void setActive(Integer active) {
		this.active = active;
	}  
	
	@OneToOne(mappedBy="member", cascade = CascadeType.ALL)
	private MemberGroup memberGroup;
	

	public MemberGroup getMemberGroup() {
		return memberGroup;
	}


	public void setMemberGroup(MemberGroup memberGroup) {
		this.memberGroup = memberGroup;
	}

}
