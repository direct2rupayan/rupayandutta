package com.mstar.web.account.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GENDER_MST")
public class Gender {
	
	@Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "GEN_ID", nullable = false)	
	 private Long genId;
	
	 @Column(name = "GEN_NAME",length = 50, nullable = false)
	 private String genName;
	 
	 @Column(name = "GEN_DESC",length = 50, nullable = false)
	 private String genDesc;

	public Long getGenId() {
		return genId;
	}

	public void setGenId(Long genId) {
		this.genId = genId;
	}

	public String getGenName() {
		return genName;
	}

	public void setGenName(String genName) {
		this.genName = genName;
	}

	public String getGenDesc() {
		return genDesc;
	}

	public void setGenDesc(String genDesc) {
		this.genDesc = genDesc;
	}

}
