package com.mstar.web.account.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name = "STUDENT")
public class Student implements Serializable {
	private static final long serialVersionUID = 1L;
	
	 public Student() {
		super();
	}
	public Student(Long sId, String enrollmentNumber, String name, String dtOfBirth, String grade, String section,
			String bloodGr, String nationality, String religion, String category, String email, String phone,
			String currentStreetAddress, String currentPostOffice, String currentPoliceStation, String currentPIN,
			String permStreetAddress, String permPostOffice, String permPoliceStation, String permPIN,String gender,Integer status,String aadhar, Parent parent) {
		super();
		this.sId = sId;
		this.enrollmentNumber = enrollmentNumber;
		this.name = name;
		this.dtOfBirth = dtOfBirth;
		this.grade = grade;
		this.section = section;
		this.bloodGr = bloodGr;
		this.nationality = nationality;
		this.religion = religion;
		this.category = category;
		this.email = email;
		this.phone = phone;
		this.currentStreetAddress = currentStreetAddress;
		this.currentPostOffice = currentPostOffice;
		this.currentPoliceStation = currentPoliceStation;
		this.currentPIN = currentPIN;
		this.permStreetAddress = permStreetAddress;
		this.permPostOffice = permPostOffice;
		this.permPoliceStation = permPoliceStation;
		this.permPIN = permPIN;
		this.gender=gender;
		this.aadhar=aadhar;
		this.status=status;
		this.parent = parent;
	}



	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "ST_ID", nullable = false)	
	 private Long sId;
	 
	 @Column(name = "ST_ENROLLMENT",length = 100, nullable = false)
	 private String enrollmentNumber;
	 
	 
	 @Column(name = "ST_NAME",length = 200, nullable = false)
	 private String name;
	 
	 @Column(name = "ST_DOB",length = 100, nullable = false)
	 private String dtOfBirth;
	 
	 @Column(name = "ST_GRADE",length = 100, nullable = true)
	 private String grade;
	 
	 @Column(name = "ST_SECTION",length = 100, nullable = true)
	 private String section;
	 
	 @Column(name = "ST_BLOOD_GR",length = 100, nullable = false)
	 private String bloodGr;	 
	 
	 @Column(name = "ST_NATIONALITY",length = 100, nullable = false)
	 private String nationality;
	 
	 
	 @Column(name = "ST_RELIGION",length = 100, nullable = false)
	 private String religion;
	 
	 @Column(name = "ST_CATEGORY",length = 100, nullable = false)
	 private String category;
	 
	 @Column(name = "ST_EMAIL",length = 100, nullable = true)
	 private String email;
	 
	 @Column(name = "ST_PHONE",length = 100, nullable = false)
	 private String phone;
	 
	 @Column(name = "ST_CURNT_STR_ADDR",length = 300, nullable = false)
	 private String currentStreetAddress;
	 
	 @Column(name = "ST_CURNT_POST_OFFICE",length = 100, nullable = false)
	 private String currentPostOffice;
	 
	 @Column(name = "ST_CURNT_POLICE_STATION",length = 100, nullable = false)
	 private String currentPoliceStation;
	 
	 @Column(name = "ST_CURNT_PIN",length = 100, nullable = false)
	 private String currentPIN;
	 
	 
	 @Column(name = "ST_PERM_STR_ADDR",length = 300, nullable = false)
	 private String permStreetAddress;
	 
	 @Column(name = "ST_PERM_POST_OFFICE",length = 100, nullable = false)
	 private String permPostOffice;
	 
	 @Column(name = "ST_PERM_POLICE_STATION",length = 100, nullable = false)
	 private String permPoliceStation;
	 
	 @Column(name = "ST_PERM_PIN",length = 100, nullable = false)
	 private String permPIN;
	 
	 @Column(name = "ST_GENDER",length = 1, nullable = false)
	 private String gender;
	 
	 @Column(name = "ST_STATUS",length = 1, nullable = false)
	 private Integer status;
	 
	 @Column(name = "ST_AADHAR",length = 12)
	 private String aadhar;
	 
	 @Column(name = "ST_SIB_ENROLLMENT",length = 100)
	 private String sibEnrollmentNum;
	 
	 @Column(name = "ST_SIB_NAME",length = 200)
	 private String sibName;
	 
	 @Column(name = "ST_SIB_GRADE",length = 100)
	 private String sibGrade;
	 
	 @Column(name = "ST_SIB_SEC",length = 100)
	 private String sibSection;
	 
	 @ManyToOne(cascade=CascadeType.ALL,optional = false)
	 @JoinColumn(name ="P_ID")
	 private Parent parent;

	public Parent getParent() {
		return parent;
	}

	public void setParent(Parent parent) {
		this.parent = parent;
	}

	public String getEnrollmentNumber() {
		return enrollmentNumber;
	}

	public void setEnrollmentNumber(String enrollmentNumber) {
		this.enrollmentNumber = enrollmentNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDtOfBirth() {
		return dtOfBirth;
	}

	public void setDtOfBirth(String dtOfBirth) {
		this.dtOfBirth = dtOfBirth;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getBloodGr() {
		return bloodGr;
	}

	public void setBloodGr(String bloodGr) {
		this.bloodGr = bloodGr;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCurrentStreetAddress() {
		return currentStreetAddress;
	}

	public void setCurrentStreetAddress(String currentStreetAddress) {
		this.currentStreetAddress = currentStreetAddress;
	}

	public String getCurrentPostOffice() {
		return currentPostOffice;
	}

	public void setCurrentPostOffice(String currentPostOffice) {
		this.currentPostOffice = currentPostOffice;
	}

	public String getCurrentPoliceStation() {
		return currentPoliceStation;
	}

	public void setCurrentPoliceStation(String currentPoliceStation) {
		this.currentPoliceStation = currentPoliceStation;
	}

	public String getCurrentPIN() {
		return currentPIN;
	}

	public void setCurrentPIN(String currentPIN) {
		this.currentPIN = currentPIN;
	}

	public String getPermStreetAddress() {
		return permStreetAddress;
	}

	public void setPermStreetAddress(String permStreetAddress) {
		this.permStreetAddress = permStreetAddress;
	}

	public String getPermPostOffice() {
		return permPostOffice;
	}

	public void setPermPostOffice(String permPostOffice) {
		this.permPostOffice = permPostOffice;
	}

	public String getPermPoliceStation() {
		return permPoliceStation;
	}

	public void setPermPoliceStation(String permPoliceStation) {
		this.permPoliceStation = permPoliceStation;
	}

	public String getPermPIN() {
		return permPIN;
	}

	public void setPermPIN(String permPIN) {
		this.permPIN = permPIN;
	}

	public Long getsId() {
		return sId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dtOfBirth == null) ? 0 : dtOfBirth.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((enrollmentNumber == null) ? 0 : enrollmentNumber.hashCode());
		result = prime * result + ((grade == null) ? 0 : grade.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((sId == null) ? 0 : sId.hashCode());
		result = prime * result + ((section == null) ? 0 : section.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (dtOfBirth == null) {
			if (other.dtOfBirth != null)
				return false;
		} else if (!dtOfBirth.equals(other.dtOfBirth))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (enrollmentNumber == null) {
			if (other.enrollmentNumber != null)
				return false;
		} else if (!enrollmentNumber.equals(other.enrollmentNumber))
			return false;
		if (grade == null) {
			if (other.grade != null)
				return false;
		} else if (!grade.equals(other.grade))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (sId == null) {
			if (other.sId != null)
				return false;
		} else if (!sId.equals(other.sId))
			return false;
		if (section == null) {
			if (other.section != null)
				return false;
		} else if (!section.equals(other.section))
			return false;
		return true;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Integer getStatus() {
		return status;
	}
	public String getSibEnrollmentNum() {
		return sibEnrollmentNum;
	}
	public void setSibEnrollmentNum(String sibEnrollmentNum) {
		this.sibEnrollmentNum = sibEnrollmentNum;
	}
	public String getSibName() {
		return sibName;
	}
	public void setSibName(String sibName) {
		this.sibName = sibName;
	}
	public String getSibGrade() {
		return sibGrade;
	}
	public void setSibGrade(String sibGrade) {
		this.sibGrade = sibGrade;
	}
	public String getSibSection() {
		return sibSection;
	}
	public void setSibSection(String sibSection) {
		this.sibSection = sibSection;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getAadhar() {
		return aadhar;
	}
	public void setAadhar(String aadhar) {
		this.aadhar = aadhar;
	}

}
