package com.mstar.web.account.entity;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EVENT")
public class Event implements Serializable {

	@Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "EVENT_ID", nullable = false)	
	 private Long eventId;
	
	 @Column(name = "EVENT_NAME",length = 250, nullable = false)
	 private String eventName;
	 
	 @Column(name = "EVENT_DESCRIPTION",length = 350, nullable = false)
	 private String eventDescription;
	 
	 @Column(name = "EVENT_START",length = 350, nullable = false)
	 private LocalDateTime eventStart;
	 
	 @Column(name = "EVENT_END",length = 350, nullable = false)
	 private LocalDateTime eventend;
	 
	 @Column(name = "CREATED_BY",length = 200, nullable = false)
	 private String createdBy;
	 
	 @Column(name = "CREATION_DT",length = 200, nullable = false)
	 private Date creationDt;
	 
	 @Column(name = "EVENT_RECEIVERS",length = 10, nullable = false)
	 private String receivers;

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public LocalDateTime getEventStart() {
		return eventStart;
	}

	public void setEventStart(LocalDateTime eventStart) {
		this.eventStart = eventStart;
	}

	public LocalDateTime getEventend() {
		return eventend;
	}

	public void setEventend(LocalDateTime eventend) {
		this.eventend = eventend;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDt() {
		return creationDt;
	}

	public void setCreationDt(Date creationDt) {
		this.creationDt = creationDt;
	}

	public String getReceivers() {
		return receivers;
	}

	public void setReceivers(String receivers) {
		this.receivers = receivers;
	}
}
