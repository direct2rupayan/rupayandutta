package com.mstar.web.account.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "INSTRUCTOR")
public class Instructor implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(name = "INS_ID", nullable = false)	
private Long insId;

@Column(name = "EMP_NUMBER",length = 100, nullable = false)
private String employmentNumber;

@Column(name = "INS_NAME",length = 200, nullable = false)
private String name;

@Column(name = "INS_DOB",length = 100, nullable = false)
private String dtOfBirth;

@Column(name = "INS_BLOOD_GR",length = 100, nullable = false)
private String bloodGr;	 

@Column(name = "INS_NATIONALITY",length = 100, nullable = false)
private String nationality;


@Column(name = "INS_RELIGION",length = 100, nullable = false)
private String religion;

@Column(name = "INS_CATEGORY",length = 100, nullable = false)
private String category;

@Column(name = "INS_EMAIL",length = 100, nullable = true)
private String email;

@Column(name = "INS_PHONE",length = 100, nullable = false)
private String phone;

@Column(name = "INS_CURNT_STR_ADDR",length = 300, nullable = false)
private String currentStreetAddress;

@Column(name = "INS_CURNT_POST_OFFICE",length = 100, nullable = false)
private String currentPostOffice;

@Column(name = "INS_CURNT_POLICE_STATION",length = 100, nullable = false)
private String currentPoliceStation;

@Column(name = "INS_CURNT_PIN",length = 100, nullable = false)
private String currentPIN;


@Column(name = "INS_PERM_STR_ADDR",length = 300, nullable = false)
private String permStreetAddress;

@Column(name = "INS_PERM_POST_OFFICE",length = 100, nullable = false)
private String permPostOffice;

@Column(name = "INS_PERM_POLICE_STATION",length = 100, nullable = false)
private String permPoliceStation;

@Column(name = "INS_PERM_PIN",length = 100, nullable = false)
private String permPIN;

@Column(name = "INS_PRIM_SUBJECTS",length = 300, nullable = false)
private String primarySubjects;

@Column(name = "INS_SEC_SUBJECTS",length = 300, nullable = false)
private String secondarySubjects;

@Column(name = "INS_GRADES",length = 300, nullable = false)
private String teachingInGrades;

@Column(name = "INS_GENDER",length = 1, nullable = false)
private String gender;

@Column(name = "INS_STATUS",nullable = false)
private Integer status;

@Column(name = "INS_AADHAR",length = 12)
private String aadhar;

public String getEmploymentNumber() {
	return employmentNumber;
}

public void setEmploymentNumber(String employmentNumber) {
	this.employmentNumber = employmentNumber;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getDtOfBirth() {
	return dtOfBirth;
}

public void setDtOfBirth(String dtOfBirth) {
	this.dtOfBirth = dtOfBirth;
}

public String getBloodGr() {
	return bloodGr;
}

public void setBloodGr(String bloodGr) {
	this.bloodGr = bloodGr;
}

public String getNationality() {
	return nationality;
}

public void setNationality(String nationality) {
	this.nationality = nationality;
}

public String getReligion() {
	return religion;
}

public void setReligion(String religion) {
	this.religion = religion;
}

public String getCategory() {
	return category;
}

public void setCategory(String category) {
	this.category = category;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getPhone() {
	return phone;
}

public void setPhone(String phone) {
	this.phone = phone;
}

public String getCurrentStreetAddress() {
	return currentStreetAddress;
}

public void setCurrentStreetAddress(String currentStreetAddress) {
	this.currentStreetAddress = currentStreetAddress;
}

public String getCurrentPostOffice() {
	return currentPostOffice;
}

public void setCurrentPostOffice(String currentPostOffice) {
	this.currentPostOffice = currentPostOffice;
}

public String getCurrentPoliceStation() {
	return currentPoliceStation;
}

public void setCurrentPoliceStation(String currentPoliceStation) {
	this.currentPoliceStation = currentPoliceStation;
}

public String getCurrentPIN() {
	return currentPIN;
}

public void setCurrentPIN(String currentPIN) {
	this.currentPIN = currentPIN;
}

public String getPermStreetAddress() {
	return permStreetAddress;
}

public void setPermStreetAddress(String permStreetAddress) {
	this.permStreetAddress = permStreetAddress;
}

public String getPermPostOffice() {
	return permPostOffice;
}

public void setPermPostOffice(String permPostOffice) {
	this.permPostOffice = permPostOffice;
}

public String getPermPoliceStation() {
	return permPoliceStation;
}

public void setPermPoliceStation(String permPoliceStation) {
	this.permPoliceStation = permPoliceStation;
}

public String getPermPIN() {
	return permPIN;
}

public void setPermPIN(String permPIN) {
	this.permPIN = permPIN;
}

public String getPrimarySubjects() {
	return primarySubjects;
}

public void setPrimarySubjects(String primarySubjects) {
	this.primarySubjects = primarySubjects;
}

public String getSecondarySubjects() {
	return secondarySubjects;
}

public void setSecondarySubjects(String secondarySubjects) {
	this.secondarySubjects = secondarySubjects;
}

public String getTeachingInGrades() {
	return teachingInGrades;
}

public void setTeachingInGrades(String teachingInGrades) {
	this.teachingInGrades = teachingInGrades;
}

public Long getInsId() {
	return insId;
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((dtOfBirth == null) ? 0 : dtOfBirth.hashCode());
	result = prime * result + ((email == null) ? 0 : email.hashCode());
	result = prime * result + ((employmentNumber == null) ? 0 : employmentNumber.hashCode());
	result = prime * result + ((getInsId() == null) ? 0 : getInsId().hashCode());
	result = prime * result + ((name == null) ? 0 : name.hashCode());
	result = prime * result + ((phone == null) ? 0 : phone.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Instructor other = (Instructor) obj;
	if (dtOfBirth == null) {
		if (other.dtOfBirth != null)
			return false;
	} else if (!dtOfBirth.equals(other.dtOfBirth))
		return false;
	if (email == null) {
		if (other.email != null)
			return false;
	} else if (!email.equals(other.email))
		return false;
	if (employmentNumber == null) {
		if (other.employmentNumber != null)
			return false;
	} else if (!employmentNumber.equals(other.employmentNumber))
		return false;
	if (getInsId() == null) {
		if (other.getInsId() != null)
			return false;
	} else if (!getInsId().equals(other.getInsId()))
		return false;
	if (name == null) {
		if (other.name != null)
			return false;
	} else if (!name.equals(other.name))
		return false;
	if (phone == null) {
		if (other.phone != null)
			return false;
	} else if (!phone.equals(other.phone))
		return false;
	return true;
}

public Integer getStatus() {
	return status;
}

public void setStatus(Integer status) {
	this.status = status;
}

public String getGender() {
	return gender;
}

public void setGender(String gender) {
	this.gender = gender;
}

public void setInsId(Long insId) {
	this.insId = insId;
}

public String getAadhar() {
	return aadhar;
}

public void setAadhar(String aadhar) {
	this.aadhar = aadhar;
}

}
