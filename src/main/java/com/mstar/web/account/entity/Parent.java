package com.mstar.web.account.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PARENT")
public class Parent implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public Parent() {
		super();
	}

	public Parent(Long pId, String pName, String pRelation, String pOccupation, String pEmail, String pPhone) {
		super();
		this.pId = pId;
		this.pName = pName;
		this.pRelation = pRelation;
		this.pOccupation = pOccupation;
		this.pEmail = pEmail;
		this.pPhone = pPhone;
	}





	@Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "P_ID", nullable = false)	
	 private Long pId;
	 
	 @Column(name = "P_NAME",length = 200, nullable = false)
	 private String pName;
	 
	 @Column(name = "P_RELATION",length = 100, nullable = false)
	 private String pRelation;
	 
	 @Column(name = "P_OCCUPATION",length = 100, nullable = false)
	 private String pOccupation;
	 
	 @Column(name = "P_EMAIL",length = 100, nullable = false)
	 private String pEmail;
	 
	 @Column(name = "P_PHONE",length = 100, nullable = false)
	 private String pPhone;

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public String getpRelation() {
		return pRelation;
	}

	public void setpRelation(String pRelation) {
		this.pRelation = pRelation;
	}

	public String getpOccupation() {
		return pOccupation;
	}

	public void setpOccupation(String pOccupation) {
		this.pOccupation = pOccupation;
	}

	public String getpEmail() {
		return pEmail;
	}

	public void setpEmail(String pEmail) {
		this.pEmail = pEmail;
	}

	public String getpPhone() {
		return pPhone;
	}

	public void setpPhone(String pPhone) {
		this.pPhone = pPhone;
	}

	public Long getpId() {
		return pId;
	}
	
	public void setpId(Long pId) {
		this.pId = pId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pEmail == null) ? 0 : pEmail.hashCode());
		result = prime * result + ((pId == null) ? 0 : pId.hashCode());
		result = prime * result + ((pName == null) ? 0 : pName.hashCode());
		result = prime * result + ((pOccupation == null) ? 0 : pOccupation.hashCode());
		result = prime * result + ((pPhone == null) ? 0 : pPhone.hashCode());
		result = prime * result + ((pRelation == null) ? 0 : pRelation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parent other = (Parent) obj;
		if (pEmail == null) {
			if (other.pEmail != null)
				return false;
		} else if (!pEmail.equals(other.pEmail))
			return false;
		if (pId == null) {
			if (other.pId != null)
				return false;
		} else if (!pId.equals(other.pId))
			return false;
		if (pName == null) {
			if (other.pName != null)
				return false;
		} else if (!pName.equals(other.pName))
			return false;
		if (pOccupation == null) {
			if (other.pOccupation != null)
				return false;
		} else if (!pOccupation.equals(other.pOccupation))
			return false;
		if (pPhone == null) {
			if (other.pPhone != null)
				return false;
		} else if (!pPhone.equals(other.pPhone))
			return false;
		if (pRelation == null) {
			if (other.pRelation != null)
				return false;
		} else if (!pRelation.equals(other.pRelation))
			return false;
		return true;
	}

}
