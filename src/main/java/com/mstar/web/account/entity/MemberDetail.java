package com.mstar.web.account.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "MEMBER_DETAIL")
public class MemberDetail implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "MEMBER_DETAIL_ID", nullable = false)	
	private Long memberDetailId;
	
	@Column(name = "MEMBER_ID", nullable = false)
	private Long memberId;
	
	public long getMemberDetailId() {
		return memberDetailId;
	}
	public void setMemberDetailId(Long memberDetailId) {
		this.memberDetailId = memberDetailId;
	}
	public long getMemberId() {
		return memberId;
	}
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	@OneToOne(targetEntity=Member.class,cascade=CascadeType.ALL)  
	@JoinColumn(name="MEMBER_ID",insertable=false,updatable=false)
	/*@OneToOne(mappedBy="memberDetail",cascade=CascadeType.ALL)  
	@JoinColumn(name="MEMBER_ID")*/
	private Member member;  
	
	public Member getMember() {
		return member;
	}
	public void setMember(Member member) {
		this.member = member;
	}

}
