package com.mstar.web.account.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SECTION_MST")
public class Section {
	
	@Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "SECN_ID", nullable = false)	
	 private Long secnId;
	
	 @Column(name = "SECN_NAME",length = 50, nullable = false)
	 private String secnName;
	 
	 @Column(name = "SECN_DESC",length = 50, nullable = false)
	 private String secnDesc;

	public Long getSecnId() {
		return secnId;
	}

	public void setSecnId(Long secnId) {
		this.secnId = secnId;
	}

	public String getSecnName() {
		return secnName;
	}

	public void setSecnName(String secnName) {
		this.secnName = secnName;
	}

	public String getSecnDesc() {
		return secnDesc;
	}

	public void setSecnDesc(String secnDesc) {
		this.secnDesc = secnDesc;
	}
	 
	 

}
