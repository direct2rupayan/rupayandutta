package com.mstar.web.account.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CATEGORY_MST")
public class Category {
	
	@Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "CAT_ID", nullable = false)	
	 private Long catId;
	
	 @Column(name = "CAT_NAME",length = 50, nullable = false)
	 private String catName;
	 
	 @Column(name = "CAT_DESC",length = 50, nullable = false)
	 private String catDesc;

	public Long getCatId() {
		return catId;
	}

	public void setCatId(Long catId) {
		this.catId = catId;
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public String getCatDesc() {
		return catDesc;
	}

	public void setCatDesc(String catDesc) {
		this.catDesc = catDesc;
	}

	
	

}
