package com.mstar.web.account.dao;

import java.util.List;

import com.mstar.web.account.entity.Category;
import com.mstar.web.account.entity.Gender;
import com.mstar.web.account.entity.Section;
import com.mstar.web.account.entity.Standard;
import com.mstar.web.account.entity.Subjects;

public interface IMasterRepository {

	public List<Standard> getStandard();
	public List<Category> getBloodgroups();
	public List<Gender> getGender();
	public List<Section> getSection();
	public List<Subjects> getSubjects();

}
