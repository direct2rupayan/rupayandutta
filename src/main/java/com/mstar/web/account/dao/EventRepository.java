package com.mstar.web.account.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mstar.web.account.entity.Events;
import com.mstar.web.account.entity.Student;

@Component
public class EventRepository implements IEventRepository {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Events> getAllEvents() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Events> list= session.createCriteria(Events.class).list();
        return list;
	}

	@Override
	public int createEvent(Events evnt) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(evnt);
		return 1;
	}

	@Override
	public List<Events> getMemberEvents(Long memberID,String typ) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Events> runstat=null;
		if("ALL".equals(typ)){
		Query findStat=session.createQuery(NativeQuery.FIND_EVENTS_BY_MEMBER_LOGIN_QUERY).setParameter(1, memberID);
		runstat=(List<Events>)findStat.getResultList();
		}else if("ME".equals(typ)){
			Query findStat=session.createQuery(NativeQuery.FIND_EVENTS_BY_MEMBER_LOGIN_QUERY2).setParameter(1, memberID);
			runstat=(List<Events>)findStat.getResultList();
		}
		return runstat;
	}

	@Override
	public Events getEventByID(Long eventId) {
		Session session = this.sessionFactory.getCurrentSession();
		Events event=(Events)session.get(Events.class, eventId);
		return event;
	}

	@Override
	public int deleteEventByID(Long eventId) {
		Session session = this.sessionFactory.getCurrentSession();
		Events event=(Events)session.get(Events.class, eventId);
		session.delete(event);
		return 1;
	}

}
