package com.mstar.web.account.dao;

import java.util.List;

import com.mstar.web.account.entity.Events;

public interface IEventRepository {

	public List<Events> getAllEvents();

	public int createEvent(Events evnt);

	public List<Events> getMemberEvents(Long memberId,String typ);

	public Events getEventByID(Long eventId);

	public int deleteEventByID(Long eventId);

}
