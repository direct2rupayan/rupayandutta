package com.mstar.web.account.dao;

import java.util.List;

import com.mstar.web.account.entity.Instructor;


public interface IInstructorRepository {
	public void addInstructor(Instructor instructor);
    public List<Instructor> getInstructor();
    public Instructor findById(long id);
    public Instructor update(Instructor instructor, long id);
    public void delete(long id);
	int changeStatus(long id, int status);
}
