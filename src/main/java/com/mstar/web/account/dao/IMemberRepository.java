package com.mstar.web.account.dao;

import com.mstar.web.account.entity.Member;

public interface IMemberRepository {

	public void addMember(Member member);
	public Long getMemberIdFromloginName(String loginName);
	
}
