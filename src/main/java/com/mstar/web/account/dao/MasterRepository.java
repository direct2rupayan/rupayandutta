package com.mstar.web.account.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mstar.web.account.entity.Category;
import com.mstar.web.account.entity.Gender;
import com.mstar.web.account.entity.Section;
import com.mstar.web.account.entity.Standard;
import com.mstar.web.account.entity.Subjects;

@Repository
public class MasterRepository implements IMasterRepository{
	
	@Autowired
	private SessionFactory sessionFactory;


	@Override
	public List<Standard> getStandard() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Standard> list= session.createCriteria(Standard.class).list();
        return list;
	}
	
	@Override
	public List<Section> getSection() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Section> list= session.createCriteria(Section.class).list();
        return list;
	}
	
	@Override
	public List<Category> getBloodgroups() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Category> list= session.createCriteria(Category.class).list();
        return list;
	}
	
	@Override
	public List<Gender> getGender() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Gender> list= session.createCriteria(Gender.class).list();
        return list;
	}
	
	@Override
	public List<Subjects> getSubjects() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Subjects> list= session.createCriteria(Subjects.class).list();
        return list;
	}


}
