package com.mstar.web.account.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.mstar.web.account.entity.Member;
import com.mstar.web.account.entity.MemberDetail;

@Repository
public class MemberRepository implements IMemberRepository{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addMember(Member member) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(member);
	}

	@Override
	public Long getMemberIdFromloginName(String loginName) {
		Session session = this.sessionFactory.getCurrentSession();
		Query selectMemberId=session.createQuery(NativeQuery.FIND_MEMBER_ID_BYLOGIN_NAME_QUERY).setParameter(1, loginName);
		List<Long> runstat=selectMemberId.getResultList();
		if(runstat!=null && runstat.size()>0){
			return runstat.get(0);
		}
		return 0L;
	}

}
