/*package com.mstar.web.account.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Repository;
import com.mstar.web.account.model.Member;

@Repository
public class UserRepository implements IUserRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	public static final String DEF_USERS_BY_USERNAME_QUERY = "SELECT MEMBER_ID,LOGIN,PASSWORD,ACTIVE FROM lms.MEMBER WHERE LOGIN=?";
	public static final String DEF_GROUP_AUTHORITIES_BY_USERNAME_QUERY = 
	"SELECT G.GROUP_ID, G.GROUP_NAME, P.NAME FROM lms.MEMBER U INNER JOIN lms.MEMBER_GROUP UG on "
	+ "U.MEMBER_ID = UG.MEMBER_ID INNER JOIN lms.GROUP G ON UG.GROUP_ID = G.GROUP_ID INNER JOIN lms.GROUP_PERMISSION GP ON "
	+ "G.GROUP_ID = GP.GROUP_ID INNER JOIN lms.PERMISSION P ON GP.PERMISSION_ID = P.PERMISSION_ID WHERE U.LOGIN = ?";

	public Member findUserAccount(String login) {
		Member member = (Member) jdbcTemplate.queryForObject(DEF_USERS_BY_USERNAME_QUERY, new Object[] { login },
				new RowMapper<Member>() {
					public Member mapRow(ResultSet rs, int rowNum) throws SQLException {
						Member member = new Member();
						member.setLogin(rs.getString("LOGIN"));
						member.setEncrytedPassword(rs.getString("PASSWORD"));
						return member;
					}
				});
		return member;
	}

	public List<GrantedAuthority> loadGroupAuthorities(String login) {
         return jdbcTemplate.query(DEF_GROUP_AUTHORITIES_BY_USERNAME_QUERY, new String[]{login}, 
        		 new RowMapper<GrantedAuthority>() {
        		 public GrantedAuthority mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new SimpleGrantedAuthority(rs.getString("NAME"));
					}
         	 
	});
}
}
*/