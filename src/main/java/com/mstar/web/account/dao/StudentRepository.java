package com.mstar.web.account.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mstar.web.account.entity.Student;
import com.mstar.web.account.model.StudentDTO;

@Repository
public class StudentRepository implements IStudentRepository {
	
	public static final String STUDENT_DETAILS_QUERY ="select s.ST_NAME,c.class_id from lms.student s,"
			+ "lms.class c  where c.class_id= s.st_grade and s.st_email= :email";
	
	private static final Logger LOG = LoggerFactory.getLogger(StudentRepository.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addStudent(Student student) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(student);

	}

	@Override
	public List<Student> getStudent() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Student> list= session.createCriteria(Student.class).list();
        return list;
	}

	@Override
	public Student findById(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Student student=(Student)session.get(Student.class, id);
		return student;
	}

	@Override
	public Student update(Student student, long id) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(student);
		return student;
	}

	@Override
	public void delete(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Student student=(Student)session.get(Student.class, id);
		session.delete(student);
	}

	@Override
	public int changeStatus(long id,int status) {
		Session session = this.sessionFactory.getCurrentSession();
		Query updStat=session.createQuery(NativeQuery.UPDATE_STUDENT_STATUS_QUERY).setParameter(1, status);
		updStat.setParameter(2, id);
		int runstat=updStat.executeUpdate();
		return runstat;
	}

	@Override
	public Student getSiblingDetails(String name, String grade) {
		Session session = this.sessionFactory.getCurrentSession();
		Query findSibStat=session.createQuery(NativeQuery.FIND_STUDENT_SIBLING_QUERY).setParameter(1, name);
		findSibStat.setParameter(2, grade);
		Student sibData=null;
		try{
		sibData=(Student) findSibStat.getSingleResult();
		LOG.info("STUDENT DATA : "+sibData.toString());
		}catch(NoResultException |NonUniqueResultException nue){
			LOG.info("ERROR :"+nue.getLocalizedMessage());
		}
		return sibData;
	}
	
	@Override
	public StudentDTO findByLogIn(String email) {
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(STUDENT_DETAILS_QUERY);
		List<StudentDTO> lst = new ArrayList<StudentDTO>();
		query.setParameter("email", email);
		List<Object[]> rows = query.list();
		StudentDTO dto = null;
		for(Object[] row : rows){
			dto = new StudentDTO();
			dto.setName(row[0].toString());
			dto.setClassId(Long.parseLong(row[1].toString()));
			lst.add(dto);
		}
		return lst.get(0);
	}

}
