package com.mstar.web.account.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mstar.web.account.entity.Instructor;
import com.mstar.web.account.entity.Student;

@Repository
public class InstructorRepository implements IInstructorRepository {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addInstructor(Instructor instructor) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(instructor);
	}

	@Override
	public List<Instructor> getInstructor() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Instructor> list= session.createCriteria(Instructor.class).list();
        return list;
	}

	@Override
	public Instructor findById(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Instructor instructor=(Instructor)session.get(Instructor.class, id);
		return instructor;
	}

	@Override
	public Instructor update(Instructor instructor, long id) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(instructor);
		return instructor;
	}

	@Override
	public void delete(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Instructor instructor=(Instructor)session.get(Instructor.class, id);
		session.delete(instructor);
	}
	
	@Override
	public int changeStatus(long id,int status) {
		Session session = this.sessionFactory.getCurrentSession();
		Query updStat=session.createQuery(NativeQuery.UPDATE_INSTRUCTOR_STATUS_QUERY).setParameter(1, status);
		updStat.setParameter(2, id);
		int runstat=updStat.executeUpdate();
		return runstat;
	}
}
