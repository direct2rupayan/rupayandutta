package com.mstar.web.account.dao;

public class NativeQuery {
	
	public static final String UPDATE_STUDENT_STATUS_QUERY= "Update Student set ST_STATUS=?1 WHERE ST_ID=?2";
	public static final String UPDATE_INSTRUCTOR_STATUS_QUERY = "Update Instructor set INS_STATUS=?1 WHERE INS_ID=?2";
	public static final String FIND_STUDENT_SIBLING_QUERY = "from Student where ST_NAME=?1 and ST_GRADE=?2" ;
	public static final String FIND_MEMBER_ID_BYLOGIN_NAME_QUERY = "Select userId from Member where login=?1" ;
	public static final String FIND_EVENTS_BY_MEMBER_LOGIN_QUERY = "from Events where (CREATED_BY=?1 and EVENT_RECEIVERS ='ME') or EVENT_RECEIVERS='ALL'";
	public static final String FIND_EVENTS_BY_MEMBER_LOGIN_QUERY2 = "from Events where (CREATED_BY=?1 and EVENT_RECEIVERS ='ME')";;
}
