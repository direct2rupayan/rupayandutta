package com.mstar.web.account.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Repository;

import com.mstar.web.account.entity.Member;

import java.util.ArrayList;
import java.util.Collection;

@Repository
public class UserAuthenticationRepository implements IUserRepository{
	
public static final String DEF_GROUP_AUTHORITIES_BY_USERNAME_QUERY = 
"SELECT P.GNAME FROM lms.MEMBERS U INNER JOIN lms.MEMBER_GROUP UG on "
//+ "U.MEMBER_ID = UG.MEMBER_ID INNER JOIN lms.GROUPS G ON UG.GROUP_ID = G.GROUP_ID INNER JOIN lms.GROUP_PERMISSION GP ON "
+ "U.MEMBER_ID = UG.MEMBER_GROUP_ID INNER JOIN lms.GROUPS G ON UG.GROUP_ID = G.GROUP_ID INNER JOIN lms.GROUP_PERMISSION GP ON "
+ "G.GROUP_ID = GP.GROUP_ID INNER JOIN lms.PERMISSION P ON GP.PERMISSION_ID = P.PERMISSION_ID WHERE U.LOGIN = ?";
	
	@Autowired
	private SessionFactory sessionFactory;
	 
	@Override
	public Member findUserAccount(String login) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria crit = session.createCriteria(Member.class);
		crit.add(Restrictions.eq("login",login));
		return (Member)crit.uniqueResult();
	}

	@Override
	public List<GrantedAuthority> loadGroupAuthorities(String login) {
		Session session = this.sessionFactory.getCurrentSession();
		List<String> userRoles= session.createNativeQuery(DEF_GROUP_AUTHORITIES_BY_USERNAME_QUERY).setParameter(1, login).list();
		List<GrantedAuthority> authorities= new ArrayList<GrantedAuthority>();
		for(String role : userRoles) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}
     
}
