package com.mstar.web.account.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.GrantedAuthority;

import com.mstar.web.account.entity.Member;

public interface IUserRepository {
	public Member findUserAccount(String login);
	public List<GrantedAuthority> loadGroupAuthorities(String login);
}
