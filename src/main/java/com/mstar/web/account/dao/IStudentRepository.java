package com.mstar.web.account.dao;

import java.util.List;

import com.mstar.web.account.entity.Student;
import com.mstar.web.account.model.StudentDTO;

public interface IStudentRepository {
	public void addStudent(Student student);
    public List<Student> getStudent();
    public Student findById(long id);
    public Student update(Student student, long id);
    public void delete(long id);
    public int changeStatus(long id, int status);
	public Student getSiblingDetails(String name, String grade);
	public StudentDTO findByLogIn(String email);
}
