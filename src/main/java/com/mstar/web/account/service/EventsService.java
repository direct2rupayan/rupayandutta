package com.mstar.web.account.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mstar.web.account.dao.IEventRepository;
import com.mstar.web.account.dao.IMemberRepository;
import com.mstar.web.account.entity.Events;
import com.mstar.web.account.model.EventsDTO;
import com.mstar.web.common.utils.EventCopyUtility;

@Service("eventsService")
@Transactional
public class EventsService implements IEventsService {
	
	private static final Logger LOG = LoggerFactory.getLogger(EventsService.class);
	
	@Autowired
    private IEventRepository eventRepository;
	
	@Autowired
    private IMemberRepository memberRepository;
	
	private static final DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aaa");
	
	@Override
	public List<EventsDTO> getEvents() {
		List<EventsDTO> eventsDtoLst= new ArrayList<EventsDTO>();
		EventsDTO eventsDTO=null;  
		List<Events> eventsLst= eventRepository.getAllEvents();
		for(Events srcObj : eventsLst) {
			LOG.info("Events >"+srcObj.getEventName());
			eventsDTO=EventCopyUtility.getEventDTO(srcObj);
			LOG.info("EventsDTO >"+eventsDTO.getTitle());
			eventsDtoLst.add(eventsDTO);
		}
		return eventsDtoLst;
	}
	
	@Override
	public List<EventsDTO> getEventsforMember(String loginName,String typ) {
		Long memberId=0L;
		if(loginName!=null && !loginName.trim().isEmpty()){
			memberId=memberRepository.getMemberIdFromloginName(loginName);
		}
		List<EventsDTO> eventsDtoLst= new ArrayList<EventsDTO>();
		if(memberId>0){
			EventsDTO eventsDTO=null;  
			List<Events> eventsLst= eventRepository.getMemberEvents(memberId,typ);
			for(Events srcObj : eventsLst) {
				LOG.info("Events >"+srcObj.getEventName());
				eventsDTO=EventCopyUtility.getEventDTO(srcObj);
				LOG.info("EventsDTO >"+eventsDTO.toString());
				eventsDtoLst.add(eventsDTO);
			}
		}
		return eventsDtoLst;
	}

	@Override
	public int createEvent(String schType, String schTitle, String schDate, String schStart, String schEnd,
			String schGrade, String schSubject, String schRemarks, String schChapter,String loginName) {
		Long memberId=0L;int ret=0;
		if(loginName!=null && !loginName.trim().isEmpty()){
			memberId=memberRepository.getMemberIdFromloginName(loginName);
		}
		if(memberId>0L){
			Date startDateTM=getDateTime(schDate,schStart);
			Date endDateTM=getDateTime(schDate,schEnd);
			Date cur= new Date();
			Events evnt=new Events(schTitle,schRemarks,startDateTM,endDateTM,memberId.toString(),cur,schType,schGrade,schSubject,schChapter,schRemarks);
			LOG.info("Event create values: "+evnt.toString());
			ret=eventRepository.createEvent(evnt);
		}else{
			ret=-1;
		}
		return ret;
	}

	private static Date getDateTime(String schDate, String schStart) {
		StringBuilder sb=new StringBuilder();
		String[] tm=schStart.split(" ");
		sb.append(schDate).append(" ").append(tm[0]).append(":00:00 ").append(tm[1]);
		Date dt=null;
		try{
			LOG.info("Date and Time"+sb.toString());
			dt=df.parse(sb.toString());
			LOG.info("Date and Time* "+dt.toString());
		}catch(ParseException pe){
			LOG.info("ERROR:"+pe.getMessage());
		}
		return dt;
	}
	
	/*public static void main(String[] args){
		Date dtt=getDateTime("06/25/2020","8 AM");
		System.out.println(dtt.toString());
	}*/

	@Override
	public int modifyEvent(String schType, String schTitle, String schDate, String schStart, String schEnd,
			String schGrade, String schSubject, String schRemarks, String schChapter, String loginName,
			Long eventId) {
		Long memberId=0L;int ret=0;
		if(loginName!=null && !loginName.trim().isEmpty()){
			memberId=memberRepository.getMemberIdFromloginName(loginName);
		}
		if(memberId>0L){
			Date startDateTM=getDateTime(schDate,schStart);
			Date endDateTM=getDateTime(schDate,schEnd);
			Events evnts=eventRepository.getEventByID(eventId);
			LOG.info("Event found values: "+evnts.toString());
			evnts.setReceivers(schType);evnts.setEventSubjct(schSubject);evnts.setEventRemark(schRemarks);evnts.setEventGrade(schGrade);
			evnts.setEventChapter(schChapter);evnts.setEventDescription(schRemarks);evnts.setEventName(schTitle);
			evnts.setEventStart(startDateTM);evnts.setEventEnd(endDateTM);
			LOG.info("Event modified values: "+evnts.toString());
			ret=eventRepository.createEvent(evnts);
			}
		return ret;
	}
	
	@Override
	public int deleteEvent(Long eventId) {
		
	int ret=eventRepository.deleteEventByID(eventId);
			
	return ret;
	}

}
