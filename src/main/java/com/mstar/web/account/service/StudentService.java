package com.mstar.web.account.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mstar.web.account.controller.StudentController;
import com.mstar.web.account.dao.IStudentRepository;
import com.mstar.web.account.entity.Parent;
import com.mstar.web.account.entity.Student;
import com.mstar.web.account.model.StudentDTO;
import com.mstar.web.common.utils.StudentCloningUtil;


@Service("studentService")
@Transactional
public class StudentService implements IStudentService {
	
	private static final Logger LOG = LoggerFactory.getLogger(StudentService.class);
	
	@Autowired
    private IStudentRepository studentRepository;

	@Override
	public void addStudent(StudentDTO student) {
		Student studentEntity = new Student();
    	Parent parent  = new Parent();
    	studentEntity.setParent(parent);
    	StudentCloningUtil.fromDtoToEntity(student, studentEntity);
    	LOG.info("studentEntity: "+studentEntity.getStatus());
    	LOG.info("student: "+student.getStatus());
        studentRepository.addStudent(studentEntity);

	}

	@Override
	public List<StudentDTO> getStudent() {
		// TODO Auto-generated method stub
		List<StudentDTO> studentDtoLst= new ArrayList<StudentDTO>();
		StudentDTO studentDTO=null;  
		List<Student> studentEntityLst= studentRepository.getStudent();
		for(Student studentObj : studentEntityLst) {
			studentDTO = new StudentDTO();
			StudentCloningUtil.fromEntityToDto(studentObj, studentDTO);
			studentDtoLst.add(studentDTO);
		}
		return studentDtoLst;
	}

	@Override
	public StudentDTO findById(long id) {
		StudentDTO studentDTO=new StudentDTO();
		Student studentObj = studentRepository.findById(id);
		StudentCloningUtil.fromEntityToDto(studentObj, studentDTO);
		return studentDTO;
	}

	@Override
	public StudentDTO update(StudentDTO student, long id) {
		LOG.info("INSIDE Update of Student Service");
		Student studentObjold = studentRepository.findById(id);
    	StudentCloningUtil.fromDtoToEntityforUpdateAndView(student, studentObjold);
    	Student studentObj = studentRepository.update(studentObjold,id);
    	LOG.info("OUTSIDE Update of Student Service");
    	StudentCloningUtil.fromEntityToDto(studentObj,student );
		return student;
	}

	@Override
	public void delete(long id) {
		studentRepository.delete(id);

	}

	@Override
	public int updateStatus(long id, int status) {
		LOG.info("INSIDE Update Status of Student Service with id:"+id+" Status:"+status);
		int result=studentRepository.changeStatus(id, status);
		return result;
	}

	@Override
	public StudentDTO findSiblingDetails(String name, String grade) {
		LOG.info("INSIDE find Sibling Details Student Service with name:"+name+" grade:"+grade);
		Student stdDet=studentRepository.getSiblingDetails(name, grade);
		StudentDTO stdDetDto=null;
		if(stdDet!=null){
			stdDetDto=new StudentDTO();
			StudentCloningUtil.fromEntityToDto(stdDet,stdDetDto);
		}
		return stdDetDto;
	}
	
	public StudentDTO findByLogIn(String email) {
		return studentRepository.findByLogIn(email);
	}

}
