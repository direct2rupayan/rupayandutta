package com.mstar.web.account.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mstar.web.account.dao.IMemberRepository;
import com.mstar.web.account.entity.Member;
import com.mstar.web.account.model.MemberDTO;
import com.mstar.web.common.utils.MemberCopyUtil;

@Service("memberService")
@Transactional
public class MemberService implements IMemberService{
	
	private static final Logger LOG = LoggerFactory.getLogger(MemberService.class);
	
	@Autowired
    private IMemberRepository memberRepository;
	
	@Override
	public void addMember(MemberDTO member) {
		
		LOG.info("Inside Member Service add");
		Member members=MemberCopyUtil.getmemberEntity(member);
		memberRepository.addMember(members);
		
	}

}
