package com.mstar.web.account.service;

import java.util.List;

import com.mstar.web.account.model.EventsDTO;

public interface IEventsService {

	public List<EventsDTO> getEvents();

	public int createEvent(String schType, String schTitle, String schDate, String schStart, String schEnd, String schGrade,
			String schSubject, String schRemarks, String schChapter, String loginName);

	public List<EventsDTO> getEventsforMember(String loginName,String typ);

	public int modifyEvent(String schType, String schTitle, String schDate, String schStart, String schEnd,
			String schGrade, String schSubject, String schRemarks, String schChapter, String loginName, Long eventId);

	public int deleteEvent(Long eventId);

}
