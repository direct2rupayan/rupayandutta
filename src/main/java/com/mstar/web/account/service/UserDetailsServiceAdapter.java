package com.mstar.web.account.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mstar.web.account.dao.IUserRepository;
import com.mstar.web.account.entity.Member;



@Service("userDetailsServiceAdapter")
@Transactional
public class UserDetailsServiceAdapter implements UserDetailsService {
	@Autowired
	private IUserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		Member member= userRepository.findUserAccount(login);
		if(null==member) {
			throw new UsernameNotFoundException("User "+ member.getLogin() + " Not Found");
			
		}
		User user= new org.springframework.security.core.userdetails.User(member.getLogin(), member.getEncrytedPassword(),
				userRepository.loadGroupAuthorities(login));
		return user;
	}
}
