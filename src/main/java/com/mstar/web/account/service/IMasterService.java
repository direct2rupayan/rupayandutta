package com.mstar.web.account.service;

import java.util.List;

import com.mstar.web.account.model.MasterDTO;

public interface IMasterService {

	public List<MasterDTO> getAllStandard();
	public List<MasterDTO> getAllSection();
	public List<MasterDTO> getAllGender();
	public List<MasterDTO> getAllCategoryGrp();
	public List<MasterDTO> getAllSubjects();

}
