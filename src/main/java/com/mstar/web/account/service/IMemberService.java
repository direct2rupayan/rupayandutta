package com.mstar.web.account.service;

import com.mstar.web.account.model.MemberDTO;

public interface IMemberService {
	
	public void addMember(MemberDTO member);

}
