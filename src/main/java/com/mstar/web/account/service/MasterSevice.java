package com.mstar.web.account.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mstar.web.account.dao.IMasterRepository;
import com.mstar.web.account.entity.Category;
import com.mstar.web.account.entity.Gender;
import com.mstar.web.account.entity.Section;
import com.mstar.web.account.entity.Standard;
import com.mstar.web.account.entity.Subjects;
import com.mstar.web.account.model.MasterDTO;

@Service("masterService")
@Transactional
public class MasterSevice implements IMasterService{
	
	@Autowired
	private IMasterRepository masterRepository;
	
	@Override
	public List<MasterDTO> getAllStandard() {
		List<Standard> mstStdn=masterRepository.getStandard();
		List<MasterDTO> mstDtoLst=null;
		if(mstStdn!=null && mstStdn.size()>0){
			mstDtoLst=new ArrayList<MasterDTO>();
			for(Standard std:mstStdn){
				MasterDTO mtd=new MasterDTO(std.getStdnId().toString(),std.getStdnDesc());
				mstDtoLst.add(mtd);
			}
		}
		return mstDtoLst;
	}
	
	@Override
	public List<MasterDTO> getAllSection() {
		List<Section> mstStdn=masterRepository.getSection();
		List<MasterDTO> mstDtoLst=null;
		if(mstStdn!=null && mstStdn.size()>0){
			mstDtoLst=new ArrayList<MasterDTO>();
			for(Section std:mstStdn){
				MasterDTO mtd=new MasterDTO(std.getSecnName(),std.getSecnDesc());
				mstDtoLst.add(mtd);
			}
		}
		return mstDtoLst;
		
	}
	
	@Override
	public List<MasterDTO> getAllCategoryGrp() {
		List<Category> mstStdn=masterRepository.getBloodgroups();
		List<MasterDTO> mstDtoLst=null;
		if(mstStdn!=null && mstStdn.size()>0){
			mstDtoLst=new ArrayList<MasterDTO>();
			for(Category std:mstStdn){
				MasterDTO mtd=new MasterDTO(std.getCatName(),std.getCatDesc());
				mstDtoLst.add(mtd);
			}
		}
		return mstDtoLst;
		
	}
	
	@Override
	public List<MasterDTO> getAllGender() {
		List<Gender> mstStdn=masterRepository.getGender();
		List<MasterDTO> mstDtoLst=null;
		if(mstStdn!=null && mstStdn.size()>0){
			mstDtoLst=new ArrayList<MasterDTO>();
			for(Gender std:mstStdn){
				MasterDTO mtd=new MasterDTO(std.getGenName(),std.getGenDesc());
				mstDtoLst.add(mtd);
			}
		}
		return mstDtoLst;
	}
	
	@Override
	public List<MasterDTO> getAllSubjects() {
		List<Subjects> mstStdn=masterRepository.getSubjects();
		List<MasterDTO> mstDtoLst=null;
		if(mstStdn!=null && mstStdn.size()>0){
			mstDtoLst=new ArrayList<MasterDTO>();
			for(Subjects std:mstStdn){
				MasterDTO mtd=new MasterDTO(std.getSubId().toString(),std.getSubDesc());
				mstDtoLst.add(mtd);
			}
		}
		return mstDtoLst;
	}

}
