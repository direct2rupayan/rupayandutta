package com.mstar.web.account.service;

import java.util.List;
import com.mstar.web.account.entity.Instructor;
import com.mstar.web.account.model.InstructorDTO;


public interface IInstructorService {
	public void addInstructor(Instructor instructor);
	public List<InstructorDTO> getInstructor();
    public InstructorDTO findById(long id);
    public InstructorDTO update(InstructorDTO instructor, long id);
    public void delete(long id);
	int updateStatus(long id, int status);
}
