package com.mstar.web.account.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mstar.web.account.controller.UserManagementController;
import com.mstar.web.account.dao.IInstructorRepository;
import com.mstar.web.account.entity.Instructor;
import com.mstar.web.account.entity.Student;
import com.mstar.web.account.model.InstructorDTO;
import com.mstar.web.account.model.StudentDTO;
import com.mstar.web.common.utils.BeanUtility;
import com.mstar.web.common.utils.StudentCloningUtil;

@Service("instructorService")
@Transactional
public class InstructorService implements IInstructorService {
	
	private static final Logger LOG = LoggerFactory.getLogger(InstructorService.class);
	
	@Autowired
    private IInstructorRepository instructorRepository;
	
	@Override
	public void addInstructor(Instructor instructor) {
		instructorRepository.addInstructor(instructor);
	}

	@Override
	public List<InstructorDTO> getInstructor() {
		List<InstructorDTO> instructorDtoLst= new ArrayList<InstructorDTO>();
		InstructorDTO instructorDTO=null;  
		List<Instructor> instructorEntityLst= instructorRepository.getInstructor();
		for(Instructor instructorObj : instructorEntityLst) {
			instructorDTO = new InstructorDTO();
			LOG.info("instructorObj >"+instructorObj.getInsId());
			BeanUtility.copyProperties(instructorObj, instructorDTO, null);
			LOG.info("instructorDTO >"+instructorDTO.getInsId());
			instructorDtoLst.add(instructorDTO);
		}
		return instructorDtoLst;
	}

	@Override
	public InstructorDTO findById(long id) {
		InstructorDTO instructorDTO=new InstructorDTO();  
		Instructor instructorEntity= instructorRepository.findById(id);
		BeanUtility.copyProperties(instructorEntity, instructorDTO, null);
		return instructorDTO;
	}

	@Override
	public InstructorDTO update(InstructorDTO instructor, long id) {
		LOG.info("INSIDE Update of Instructor Service for :"+instructor.toString());
		Instructor instructorEntity = instructorRepository.findById(id);
		BeanUtility.copyProperties( instructor,instructorEntity,"insId");
		Instructor updInstObj = instructorRepository.update(instructorEntity,id);
    	BeanUtility.copyProperties(updInstObj,instructor);
    	LOG.info("OUTSIDE Update of Instructor Service for :"+instructor.toString());
		return instructor;
	}

	@Override
	public void delete(long id) {
		instructorRepository.delete(id);
		
	}
	
	@Override
	public int updateStatus(long id, int status) {
		LOG.info("INSIDE Update Status of Instructor Service with id:"+id+" Status:"+status);
		int result=instructorRepository.changeStatus(id, status);
		return result;
	}

}
