package com.mstar.web.account.service;

import java.util.List;

import com.mstar.web.account.entity.Student;
import com.mstar.web.account.model.StudentDTO;

public interface IStudentService {
	public void addStudent(StudentDTO student);
	public List<StudentDTO> getStudent();
    public StudentDTO findById(long id);
    public StudentDTO update(StudentDTO student, long id);
    public void delete(long id);
    public int updateStatus(long id,int status);
	public StudentDTO findSiblingDetails(String name, String grade);
	public StudentDTO findByLogIn(String email);
}
