package com.mstar.web.notification.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/*@Entity
@Table(name = "NOTIFICATION_MAPPER")*/
public class NotificationMapper {

	@Column(name = "NOTIFICATION_ID", nullable = false)	
	private Long notificationId;
	
	@Column(name = "MEMBER_ID", nullable = false)	
	private Long memberId;

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	
	
	
	
}
