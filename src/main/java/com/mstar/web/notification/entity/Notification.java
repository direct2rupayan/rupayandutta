package com.mstar.web.notification.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/*
 * https://platoiscoding.com/2019/03/26/spring-boot-crud-application-sql-database-manytomany-multiple-select/
 */
@Entity
@Table(name = "NOTIFICATION")
public class Notification {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "NOTIFICATION_ID", nullable = false)	
	private Long notificationId;
	
	@Column(name = "NOTIFICATION_NAME",length = 200, nullable = false)
	private String notificationName;
	
	@Column(name = "NOTIFICATION_MSG",length = 5000, nullable = false)
	private String notificationMsg;
	
	
	@Column(name = "NOTIFICATION_INTENDED_FOR",length = 50, nullable = false)
	private String notificationIntendedFor;
	
	@Column(name = "NOTIFIED_FOR",length = 5000, nullable = false)
	private String notifiedFor;
	
	@Column(name = "NOTIFICATION_ARCHIEVED",length = 10, nullable = false)
	private String archieved;
	
	@Column(name = "NOTIFICATION_CREATION_DATE",length = 10, nullable = false)
	private String creationDate;
	
/*	  @ManyToMany(fetch = FetchType.LAZY)
	    @JoinTable(name = "NOTIFICATION_MAPPER",
	            joinColumns = { @JoinColumn(name = "NOTIFICATION_ID") },
	            inverseJoinColumns = { @JoinColumn(name = "MEMBER_ID") })
	    private Set<com.mstar.web.account.entity.Member> categories = new HashSet<>();*/

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public String getNotificationName() {
		return notificationName;
	}

	public void setNotificationName(String notificationName) {
		this.notificationName = notificationName;
	}

	public String getNotificationMsg() {
		return notificationMsg;
	}

	public void setNotificationMsg(String notificationMsg) {
		this.notificationMsg = notificationMsg;
	}

	public String getNotificationIntendedFor() {
		return notificationIntendedFor;
	}

	public void setNotificationIntendedFor(String notificationIntendedFor) {
		this.notificationIntendedFor = notificationIntendedFor;
	}

	public String getArchieved() {
		return archieved;
	}

	public void setArchieved(String archieved) {
		this.archieved = archieved;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

}
