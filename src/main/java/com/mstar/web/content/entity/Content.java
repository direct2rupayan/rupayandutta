package com.mstar.web.content.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
@Entity
@Table(name = "CONTENT")
public class Content {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "CONTENT_ID", nullable = false)	
	private Long contentId;
	
	@Column(name = "CONTENT_NAME",length = 200, nullable = false)
	private String contentName;
	
	@Column(name = "SERIAL_NO",length = 10, nullable = false)
	private Integer serialNo;
	
	@Column(name = "FILE_NAME",length = 200, nullable = false)
	private String fileName;
	
	@Column(name = "FILE_TYPE",length = 200, nullable = false)
	private String fileType;
	
	@Column(name = "RESOURCE_REF",length = 200, nullable = false)
	private String resourceReference;
	
	@Column(name = "SSC_ID",nullable = false)
	private Long sectionSubsectionReference;
	
	@Column(name = "CLASS_ID", nullable = false)	
	private Long classId;
	
	@Column(name = "SUBJECT_ID", nullable = false)	
	private Long subjectId;
	
	@Column(name = "CH_ID", nullable = false)	
	private Long chapterId;	
	
	@Column(name = "LAST_UPDATED", nullable = false)	
	private java.sql.Timestamp lastUpdated;

	@Column(name = "CONTENT_CATEGORY",length = 45, nullable = false)
	private String contentCategory;
	
	
	
	public String getDateString() {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, MMMM d, yyyy");
		Date now = new Date();
		Date startDate = new Date(lastUpdated.getTime());
		if(dateFormatter.format(now).equalsIgnoreCase(dateFormatter.format(startDate))) {
			return "Today";
		}
		return dateFormatter.format(startDate);
		
	}
	

	public Long getContentId() {
		return contentId;
	}
	

	public Content() {
		super();
	}

	public Content(String contentName, Integer serialNo, String fileName, String fileType, String resourceReference,
			Long sectionSubsectionReference, Long classId, Long subjectId, Long chapterId,String contentCategory) {
		super();
		this.contentName = contentName;
		this.serialNo = serialNo;
		this.fileName = fileName;
		this.fileType = fileType;
		this.resourceReference = resourceReference;
		this.sectionSubsectionReference = sectionSubsectionReference;
		this.classId = classId;
		this.subjectId = subjectId;
		this.chapterId = chapterId;
		this.contentCategory=contentCategory;
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		this.lastUpdated = timestamp;
	}
	
	
	public Content(String contentName, Integer serialNo, String fileName, String fileType, String resourceReference,
			 Long classId, Long subjectId, Long chapterId,String contentCategory) {
		super();
		this.contentName = contentName;
		this.serialNo = serialNo;
		this.fileName = fileName;
		this.fileType = fileType;
		this.resourceReference = resourceReference;
		this.classId = classId;
		this.subjectId = subjectId;
		this.chapterId = chapterId;
		this.contentCategory=contentCategory;
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		this.lastUpdated = timestamp;
	}

	public void setContentId(Long contentId) {
		this.contentId = contentId;
	}

	public String getContentName() {
		return contentName;
	}

	public void setContentName(String contentName) {
		this.contentName = contentName;
	}

	public Integer getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(Integer serialNo) {
		this.serialNo = serialNo;
	}
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getResourceReference() {
		return resourceReference;
	}

	public void setResourceReference(String resourceReference) {
		this.resourceReference = resourceReference;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public Long getSectionSubsectionReference() {
		return sectionSubsectionReference;
	}

	public void setSectionSubsectionReference(Long sectionSubsectionReference) {
		this.sectionSubsectionReference = sectionSubsectionReference;
	}

	public Long getClassId() {
		return classId;
	}

	public void setClassId(Long classId) {
		this.classId = classId;
	}

	public Long getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}

	public Long getChapterId() {
		return chapterId;
	}

	public void setChapterId(Long chapterId) {
		this.chapterId = chapterId;
	}


	public java.sql.Timestamp getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(java.sql.Timestamp lastUpdated) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		this.lastUpdated = timestamp;
	}
	
	
	
}
