package com.mstar.web.content.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CHAPTER")
public class Chapter {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "CH_ID", nullable = false)	
	private Long chapterId;
	
	@Column(name = "CH_NAME",length = 300, nullable = false)
	private String chapterName;
	
	@Column(name = "CLASS_ID", nullable = false)	
	private Long classId;
	
	@Column(name = "SUBJECT_ID", nullable = false)	
	private Long subjectId;
	
	@Column(name = "LAST_UPDATE_TP", nullable = false)	
	private java.sql.Timestamp lastUpdated;
	
	@Column(name = "STATUS", nullable = false)	
	private String status;
	
	public Long getChapterId() {
		return chapterId;
	}

	public void setChapterId(Long chapterId) {
		this.chapterId = chapterId;
	}

	public String getChapterName() {
		return chapterName;
	}

	public void setChapterName(String chapterName) {
		this.chapterName = chapterName;
	}

	public Long getClassId() {
		return classId;
	}

	public void setClassId(Long classId) {
		this.classId = classId;
	}

	public Long getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public java.sql.Timestamp getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(java.sql.Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}	
	
}
