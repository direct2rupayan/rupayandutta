package com.mstar.web.content.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class LiveSessionVideo {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "VIDEO_ID", nullable = false)	
	private Long videoId;
	
	@Column(name = "TOPIC_NAME",length = 200, nullable = false)
	private String topicName;
	
	@Column(name = "VIDEO_FILE_NAME",length = 200, nullable = false)
	private String fileName;
	
	@Column(name = "FILE_TYPE",length = 200, nullable = false)
	private String fileType;
	
	@Column(name = "RESOURCE_REF",length = 200, nullable = false)
	private String resourceReference;
	
	
	@Column(name = "CLASS_ID", nullable = false)	
	private Long classId;
	
	@Column(name = "SUBJECT_ID", nullable = false)	
	private Long subjectId;
	
	@Column(name = "CH_ID", nullable = false)	
	private Long chapterId;
	
	@Column(name = "VIDEO_UPLOADED_ON",length = 10, nullable = false)
	private String creationDate;

	public LiveSessionVideo(String topicName, String fileName, String fileType, String resourceReference, Long classId,
			Long subjectId, Long chapterId, String creationDate) {
		super();
		this.topicName = topicName;
		this.fileName = fileName;
		this.fileType = fileType;
		this.resourceReference = resourceReference;
		this.classId = classId;
		this.subjectId = subjectId;
		this.chapterId = chapterId;
		this.creationDate = creationDate;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getResourceReference() {
		return resourceReference;
	}

	public void setResourceReference(String resourceReference) {
		this.resourceReference = resourceReference;
	}

	public Long getClassId() {
		return classId;
	}

	public void setClassId(Long classId) {
		this.classId = classId;
	}

	public Long getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}

	public Long getChapterId() {
		return chapterId;
	}

	public void setChapterId(Long chapterId) {
		this.chapterId = chapterId;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	
	
	
}
