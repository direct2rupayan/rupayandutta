package com.mstar.web.content.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CLASS")
public class Level implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "CLASS_ID", nullable = false)	
	private Long classId;
	
	@Column(name = "CLASS_NAME",length = 200, nullable = false)
	private String className;

	public Long getClassId() {
		return classId;
	}

	public void setClassId(Long classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
	
/*	 @ManyToMany(cascade = CascadeType.ALL)
	 @JoinTable(name = "CLASS_SUBJECT", joinColumns = { @JoinColumn(name = "CLASS_ID") }, 
	  inverseJoinColumns = { @JoinColumn(name = "SUBJECT_ID") })
	private List<Subject> subjects = new ArrayList<Subject>();*/




	
	

}
