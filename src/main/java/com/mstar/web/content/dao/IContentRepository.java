package com.mstar.web.content.dao;

import java.util.List;
import com.mstar.web.content.entity.Content;

public interface IContentRepository {
	
	public void addContent(Content content);
    public List<Content> getContents(Long classId,Long subjectId,Long chapterId,Long sectionSubsectionId);
    public List<Content> getContents(Long classId, Long subjectId, Long chapterId);
    public List<Content> getContents(Long classId,String contentCategory);
    public Content findById(long id);
    public Content findByContentReference(String contentReference);
    public void deleteContent(long id);
    
}
