package com.mstar.web.content.dao;

import java.util.List;
import com.mstar.web.content.entity.Chapter;

public interface IChapterRepository {
	public void addChapter(Chapter chapter);
    public List<Chapter> getChapters();
    public Chapter findById(long id);
    public List<Chapter> getChapters(Long classId,Long subjectId);
}
