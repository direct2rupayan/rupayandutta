package com.mstar.web.content.dao;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.mstar.web.content.entity.Chapter;

@Repository
public class ChapterRepository implements IChapterRepository{
	
	public static final String CHAPTERS_BY_CLASS_SUBJECT_QUERY = 
	"SELECT * FROM lms.chapter where CLASS_ID=:CLASS_ID AND "
	+ "SUBJECT_ID=:SUBJECT_ID";

	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addChapter(Chapter chapter) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(chapter);
	}

	@Override
	public List<Chapter> getChapters() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Chapter> list= session.createCriteria(Chapter.class).list();
		return list;
	}

	@Override
	public Chapter findById(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Chapter chapter=(Chapter)session.get(Chapter.class, id);
		return chapter;
	}
	
	@Override
	public List<Chapter> getChapters(Long classId,Long subjectId) {
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(CHAPTERS_BY_CLASS_SUBJECT_QUERY).addEntity(Chapter.class);
		query.setParameter("CLASS_ID", classId);
		query.setParameter("SUBJECT_ID", subjectId);
		return query.list();
}
}
