package com.mstar.web.content.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.mstar.web.content.entity.Content;

@Repository
public class ContentRepository implements IContentRepository {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addContent(Content content) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(content);
		
	}

	@Override
	public List<Content> getContents(Long classId, Long subjectId, Long chapterId, Long sectionSubsectionId) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria crit = session.createCriteria(Content.class);
		crit.add(Restrictions.eq("classId",classId))
		.add(Restrictions.eq("subjectId",subjectId))
		.add(Restrictions.eq("chapterId",chapterId))
		.add(Restrictions.eq("sectionSubsectionReference",sectionSubsectionId));
		List<Content> contents = crit.list();
		return contents;
	}
	
	@Override
	public List<Content> getContents(Long classId, Long subjectId, Long chapterId) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria crit = session.createCriteria(Content.class);
		crit.add(Restrictions.eq("classId",classId))
		.add(Restrictions.eq("subjectId",subjectId))
		.add(Restrictions.eq("chapterId",chapterId)).add(Restrictions.eq("contentCategory","ALL"));
		List<Content> contents = crit.list();
		return contents;
	}
	
	@Override
	public List<Content> getContents(Long classId,String contentCategory) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria crit = session.createCriteria(Content.class);
		
		if(null!= classId)  {
		crit.add(Restrictions.eq("classId",classId)).add(Restrictions.eq("contentCategory",contentCategory));
		}
		else {
			crit.add(Restrictions.eq("contentCategory",contentCategory));
		}
		List<Content> contents = crit.list();
		return contents;
	}
	
	
	@Override
	public Content findById(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Content content =(Content)session.get(Content.class, id);
		return content;
	}
	

	@Override
	public Content findByContentReference(String contentReference) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria crit = session.createCriteria(Content.class);
		crit.add(Restrictions.eq("resourceReference",contentReference));
		return (Content) crit.uniqueResult();
	}

	@Override
	public void deleteContent(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Content content=(Content)session.get(Content.class, id);
		session.delete(content);
	}
	
	

}
