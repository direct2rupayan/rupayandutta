package com.mstar.web.content.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import com.mstar.web.common.model.AjaxResponseBodyClass;
import com.mstar.web.content.entity.Chapter;
import com.mstar.web.content.entity.Level;
import com.mstar.web.content.entity.Subject;

@Repository
public class SubjectRepository implements ISubjectRepository {
	public static final String SUBJECTS_BY_CLASS_QUERY = "select sub.SUBJECT_ID,sub.SUBJECT_NAME from "
			+ "lms.SUBJECT sub ,lms.CLASS class,lms.CLASS_SUBJECT clsub where "
			+ "sub.SUBJECT_ID= clsub.SUBJECT_ID and clsub.CLASS_ID= class.CLASS_ID and "
			+ "class.CLASS_ID = :CLASS_ID order by class.CLASS_ID";
	
	public static final String CHAPTER_COUNT_FOR_SUBJECT_QUERY = "SELECT lc.CLASS_ID,cls.CLASS_NAME,"
			+ "lc.SUBJECT_ID,sub.SUBJECT_NAME,count(lc.CH_ID) cpc  FROM lms.CHAPTER lc, "
			+ "lms.CLASS cls,lms.SUBJECT sub where lc.CLASS_ID= cls.CLASS_ID and "
			+ "sub.SUBJECT_ID=lc.SUBJECT_ID and cls.CLASS_ID=:CLASS_ID group by lc.CLASS_ID,lc.SUBJECT_ID";
	
	public static final String SUBJECT_CHAPTER_COUNT_OF_CLASS_QUERY ="select  p.CLASS_ID AS classId,p.CLASS_NAME AS "
			+ "className,count(p.SUBJECT_ID) As subjectCount,SUM(p.cpc) AS chapterCount From "
			+ "(SELECT lc.CLASS_ID,cls.CLASS_NAME,lc.SUBJECT_ID,sub.SUBJECT_NAME,count(lc.CH_ID) cpc "
			+ "FROM lms.CHAPTER lc, lms.CLASS cls,lms.SUBJECT sub where lc.CLASS_ID= cls.CLASS_ID and "
			+ "sub.SUBJECT_ID=lc.SUBJECT_ID group by lc.CLASS_ID,lc.SUBJECT_ID) p group by p.CLASS_ID;";
	
	public static final String CHAPTER_LIST_OF_CLASS_SUBJECT_QUERY="";

	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Subject> getSubjects(Long classId) {
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(SUBJECTS_BY_CLASS_QUERY).addEntity(Subject.class);
		
		query.setParameter("CLASS_ID", classId);
		List<Subject> subjectList = query.list();
		return subjectList;
	}
	
	
	public List<Level> getAllClasses() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Level> list= session.createCriteria(Level.class).list();
		return list;
	}
	
	public List<AjaxResponseBodyClass> getClassDetails() {
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(SUBJECT_CHAPTER_COUNT_OF_CLASS_QUERY);
		List<AjaxResponseBodyClass> resLst=new ArrayList<>();
		List<Object[]> rows = query.list();
		for(Object[] row : rows){
			AjaxResponseBodyClass ajx = new AjaxResponseBodyClass(Long.parseLong(row[0].toString()),row[1].toString(),row[2].toString(),row[3].toString());
			resLst.add(ajx);
		}
		return resLst;
	}
	
	
	public List<AjaxResponseBodyClass> getChapterCountForClass(Long classId) {
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(CHAPTER_COUNT_FOR_SUBJECT_QUERY);
		query.setParameter("CLASS_ID", classId);
		List<AjaxResponseBodyClass> resLst=new ArrayList<>();
		List<Object[]> rows = query.list();
		for(Object[] row : rows){
			AjaxResponseBodyClass ajx = new AjaxResponseBodyClass(Long.parseLong(row[0].toString()),row[1].toString(),Long.parseLong(row[2].toString()),row[3].toString(),row[4].toString());
			resLst.add(ajx);
		}
		return resLst;
	}


}
