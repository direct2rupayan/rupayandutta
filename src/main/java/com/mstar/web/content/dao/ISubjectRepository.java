package com.mstar.web.content.dao;

import java.util.List;

import com.mstar.web.common.model.AjaxResponseBodyClass;
import com.mstar.web.content.entity.Level;
import com.mstar.web.content.entity.Subject;

public interface ISubjectRepository {
	public List<Subject> getSubjects(Long classId);
	public List<Level> getAllClasses();
	public List<AjaxResponseBodyClass> getClassDetails();
	public List<AjaxResponseBodyClass> getChapterCountForClass(Long classId);
}
