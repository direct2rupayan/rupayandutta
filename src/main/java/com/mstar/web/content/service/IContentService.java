package com.mstar.web.content.service;

import java.util.List;
import java.util.Map;

import com.mstar.web.content.entity.Content;

public interface IContentService {
	public void addContent(Content content);
    public List<Content> getContents(Long classId,Long subjectId,Long chapterId,Long sectionSubsectionId);
    public Map<String, List<Content>> getContents(Long classId,String contentCategory);
    public Content findById(long id);
    public Content findByContentReference(String contentReference);
    public void deleteContent(long id);
    public Map<Long, List<Content>> getContents(Long classId, Long subjectId, Long chapterId);

}
