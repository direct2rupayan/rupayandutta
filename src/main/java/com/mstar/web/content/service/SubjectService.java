package com.mstar.web.content.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mstar.web.content.entity.Level;
import com.mstar.web.common.model.AjaxResponseBodyClass;
import com.mstar.web.content.dao.ISubjectRepository;
import com.mstar.web.content.entity.Subject;

@Service("subjectService")
@Transactional
public class SubjectService implements ISubjectService {
	@Autowired
	private ISubjectRepository subjectRepository;

	@Override
	public List<Subject> getSubjects(Long classId) {
		return subjectRepository.getSubjects(classId);
	}
	
	@Override
	public List<Level> getAllClasses() {
		return subjectRepository.getAllClasses();
	}
	
	public List<AjaxResponseBodyClass> getClassDetails() {
		return subjectRepository.getClassDetails();
	}
	
	public List<AjaxResponseBodyClass> getChapterCountForClass(Long classId) {
		return subjectRepository.getChapterCountForClass(classId);
	}

}
