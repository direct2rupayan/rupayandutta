package com.mstar.web.content.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mstar.web.content.dao.IContentRepository;
import com.mstar.web.content.entity.Content;

@Service("contentService")
@Transactional
public class ContentService implements IContentService {
	
	@Autowired
	private IContentRepository contentRepository;

	@Override
	public void addContent(Content content) {
		contentRepository.addContent(content);
	}

	@Override
	public List<Content> getContents(Long classId, Long subjectId, Long chapterId, Long sectionSubsectionId) {
		return contentRepository.getContents(classId, subjectId, chapterId, sectionSubsectionId);
	}
	
	@Override
	public Map<String, List<Content>> getContents(Long classId,String contentCategory) {
		List<Content> contents= contentRepository.getContents(classId, contentCategory);
		Map<String, List<Content>> contentMap = new HashMap<String, List<Content>>();
		List<Content> contentList= null;
		for(Content content : contents) {
			if(!contentMap.containsKey(content.getDateString())){
				contentList = new ArrayList<Content>();
				contentList.add(content);
				contentMap.put(content.getDateString(), contentList);
			}
			else {
				contentMap.get(content.getDateString()).add(content);
			}
			
		}
	    return contentMap;
	}
	
	@Override
	public Map<Long, List<Content>> getContents(Long classId, Long subjectId, Long chapterId) {
		List<Content> contents= contentRepository.getContents(classId, subjectId,chapterId);
		Map<Long, List<Content>> contentMap = new HashMap<Long, List<Content>>();
		List<Content> contentList= null;
		for(Content content : contents) {
			if(!contentMap.containsKey(content.getSectionSubsectionReference())){
				contentList = new ArrayList<Content>();
				contentList.add(content);
				contentMap.put(content.getSectionSubsectionReference(), contentList);
			}
			else {
				contentMap.get(content.getSectionSubsectionReference()).add(content);
			}
			
		}
	    return contentMap;
	}


	@Override
	public Content findById(long id) {
       return contentRepository.findById(id);
	}
	
	

	@Override
	public Content findByContentReference(String contentReference) {
		 return contentRepository.findByContentReference(contentReference);
	}

	@Override
	public void deleteContent(long id) {
		contentRepository.deleteContent(id);
	}

}
