package com.mstar.web.content.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mstar.web.content.dao.IChapterRepository;
import com.mstar.web.content.entity.Chapter;

@Service("chapterService")
@Transactional
public class ChapterService implements IChapterService {
	
	@Autowired
    private IChapterRepository chapterRepository;

	@Override
	public void addChapter(Chapter chapter) {
		chapterRepository.addChapter(chapter);
	}

	@Override
	public List<Chapter> getChapters() {
		return chapterRepository.getChapters();
	}

	@Override
	public Chapter findById(long id) {
		return chapterRepository.findById(id);
	}
	
	@Override
	 public List<Chapter> getChapters(Long classId,Long subjectId) {
		 return chapterRepository.getChapters(classId, subjectId);
	 }

}
