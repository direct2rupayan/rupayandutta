package com.mstar.web.content.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.mstar.web.content.entity.Content;
import com.mstar.web.content.service.IContentService;

@RestController
public class ContentController {
    
    @Autowired
    private IContentService contentService;
    
	@GetMapping(value="/getObjectives" , headers="Accept=application/json")
	public List<Content> getObjectiveContents(@RequestParam(name = "classId") Long classId,@RequestParam(name = "subjectId") Long subjectId
			,@RequestParam(name = "chapterId") Long chapterId,@RequestParam(name = "sectionSubsectionId") Long sectionSubsectionId) throws Exception {
		List<Content> objectiveList = contentService.getContents(classId, subjectId, chapterId, sectionSubsectionId);
		return objectiveList;
	}
	
	
	@GetMapping(value="/liveSessions" , headers="Accept=application/json")
	public Map<String, List<Content>> getLiveSessions(@RequestParam(name = "classId") Long classId,@RequestParam(name = "contentCategory") String contentCategory) throws Exception {
		Map<String, List<Content>> liveSessionContents = contentService.getContents(classId,contentCategory);
		return liveSessionContents;
	}
	
	
	

}
