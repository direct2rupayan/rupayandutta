package com.mstar.web.content.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import com.mstar.web.content.entity.Chapter;
import com.mstar.web.content.service.IChapterService;

@RestController
public class ChapterController {
	@Autowired
	private IChapterService chapterService;
	
	@GetMapping(value="/getAllChapters" , headers="Accept=application/json")
	public List<Chapter> chapters() throws Exception {
		List<Chapter> chapterList = chapterService.getChapters();
		return chapterList;
	}
	
	@GetMapping(value="/getChaptersByClassIdSubjectId" , headers="Accept=application/json")
	public List<Chapter> chaptersByClassSubject(@RequestParam(name = "classId") Long classId,@RequestParam(name = "subjectId") Long subjectId) throws Exception {
		List<Chapter> chapterList = chapterService.getChapters(classId, subjectId);
		return chapterList;
	}
	
    @PostMapping(value="/createChapter",headers="Accept=application/json")
    public ResponseEntity<?> createChapter(@RequestBody Chapter chapter, UriComponentsBuilder ucBuilder){	
    	 chapterService.addChapter(chapter);
         HttpHeaders headers = new HttpHeaders();
        //headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
         return new ResponseEntity<String>("Chapter Created", HttpStatus.OK);
        
    }
}
