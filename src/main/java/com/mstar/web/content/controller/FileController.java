package com.mstar.web.content.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.mstar.web.common.model.UploadFileResponse;
import com.mstar.web.common.utils.UUIDClockSequence;
import com.mstar.web.content.entity.Content;
import com.mstar.web.content.service.FileStorageService;
import com.mstar.web.content.service.IContentService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@RestController
public class FileController {

    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private FileStorageService fileStorageService;
    
    @Autowired
    private IContentService contentService;
    
    String uuid= null;

    @PostMapping("/uploadFile")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file,@RequestParam("heading") String heading,@RequestParam("classId") String classId	
    ,@RequestParam("subjectId") String subjectId,@RequestParam("chapterId") String chapterId,
     @RequestParam("sectionSubsectionId") String sectionSubsectionId) {

    	uuid= UUIDClockSequence.generateUUID();
        String fileName = fileStorageService.storeFile(file,uuid);
        /*System.out.println("FileName From Storage service: " + fileName);*/
        file.getContentType();
        /*System.out.println("File Content Type : " + file.getContentType());*/

        String separator = "\\";
        String[] arrValues = fileName.split(Pattern.quote(separator));
        
        System.out.println("FileName Parts : " + arrValues.toString());
        contentService.addContent(new Content(heading, 2, arrValues[1], file.getContentType(), uuid,
        		Long.parseLong(sectionSubsectionId),Long.parseLong(classId),Long.parseLong(subjectId),Long.parseLong(chapterId),"ALL"));
        
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path("/"+ arrValues[0]+ "/")
                .path(arrValues[1])
                .toUriString();
        /*System.out.println("FileName fileDownloadUri : " + fileDownloadUri);*/
        return new UploadFileResponse(fileName, fileDownloadUri,
                file.getContentType(), file.getSize());
    }
    
    
    @PostMapping("/uploadLiveSession")
    public UploadFileResponse uploadLiveSessionFile(@RequestParam("file") MultipartFile file,@RequestParam("topicName") String topicName,@RequestParam("classId") String classId	
    ,@RequestParam("subjectId") String subjectId,@RequestParam("chapterId") String chapterId) { 

    	uuid= UUIDClockSequence.generateUUID();
        String fileName = fileStorageService.storeFile(file,uuid);
        file.getContentType();
        String separator = "\\";
        String[] arrValues = fileName.split(Pattern.quote(separator));
        
        contentService.addContent(new Content(topicName, 2, arrValues[1], file.getContentType(), uuid,
        Long.parseLong(classId),Long.parseLong(subjectId),Long.parseLong(chapterId),"LS"));
        
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path("/"+ arrValues[0]+ "/")
                .path(arrValues[1])
                .toUriString();

        return new UploadFileResponse(fileName, fileDownloadUri,
                file.getContentType(), file.getSize());
    }
    


    /*@GetMapping("/downloadFile/{fileName:.+}")*/
    @GetMapping("/downloadFile")
    public ResponseEntity<Resource> downloadFile(@RequestParam("resourceReference") String resourceReference, HttpServletRequest request) {
        // Load file as Resource
    	Content content = contentService.findByContentReference(resourceReference);
        Resource resource = fileStorageService.loadFileAsResource(resourceReference,content.getFileName());

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                //.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
    
    

}
