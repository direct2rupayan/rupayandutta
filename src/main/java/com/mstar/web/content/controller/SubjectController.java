package com.mstar.web.content.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.mstar.web.content.entity.Subject;
import com.mstar.web.common.model.AjaxResponseBodyClass;
import com.mstar.web.content.entity.Level;
import com.mstar.web.content.service.ISubjectService;

@RestController
public class SubjectController {
	
	@Autowired
	private ISubjectService subjectService;
	
	@GetMapping(value="/getAllClasses" , headers="Accept=application/json")
	public List<Level> getAllClasses() throws Exception {
		List<Level> classList = subjectService.getAllClasses();
		return classList;
	}

	
	@GetMapping(value="/getAllSubjects" , headers="Accept=application/json")
	public List<Subject> getAllSubjects(@RequestParam(name = "classId") Long classId) throws Exception {
		List<Subject> subjectList = subjectService.getSubjects(classId);
		return subjectList;
	}
	
	@GetMapping(value="/getClassDetails" , headers="Accept=application/json")
	public List<AjaxResponseBodyClass> getClassDetails() {
		return subjectService.getClassDetails();
	}
	
}


