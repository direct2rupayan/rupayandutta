package com.mstar.web.common.model;

public class AjaxResponseBodySubject {
	 private Long classId;
	 private String classNameName;
	 private Long subjectId;
	 private String subjectName;
	 private Integer chapterCount;
	 
	public Long getClassId() {
		return classId;
	}
	public void setClassId(Long classId) {
		this.classId = classId;
	}
	public String getClassNameName() {
		return classNameName;
	}
	public void setClassNameName(String classNameName) {
		this.classNameName = classNameName;
	}
	public Long getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public Integer getChapterCount() {
		return chapterCount;
	}
	public void setChapterCount(Integer chapterCount) {
		this.chapterCount = chapterCount;
	}
}
