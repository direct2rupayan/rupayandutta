package com.mstar.web.common.model;

public class AjaxResponseBodyClass {
	
private Long    classId;
private String  className;
private Long    subjectId;
private String  subjectName;
private String  subjectCount;
private String  chapterCount;

public AjaxResponseBodyClass() {
	super();
}


public AjaxResponseBodyClass(Long classId, String className, Long subjectId, String subjectName, String chapterCount) {
	super();
	this.classId = classId;
	this.className = className;
	this.subjectId = subjectId;
	this.subjectName = subjectName;
	this.chapterCount = chapterCount;
}



public AjaxResponseBodyClass(Long classId, String className, String subjectCount, String chapterCount) {
	super();
	this.classId = classId;
	this.className = className;
	this.subjectCount = subjectCount;
	this.chapterCount = chapterCount;
}


public Long getClassId() {
	return classId;
}


public void setClassId(Long classId) {
	this.classId = classId;
}


public String getClassName() {
	return className;
}


public void setClassName(String className) {
	this.className = className;
}


public Long getSubjectId() {
	return subjectId;
}


public void setSubjectId(Long subjectId) {
	this.subjectId = subjectId;
}


public String getSubjectName() {
	return subjectName;
}


public void setSubjectName(String subjectName) {
	this.subjectName = subjectName;
}


public String getSubjectCount() {
	return subjectCount;
}


public void setSubjectCount(String subjectCount) {
	this.subjectCount = subjectCount;
}


public String getChapterCount() {
	return chapterCount;
}


public void setChapterCount(String chapterCount) {
	this.chapterCount = chapterCount;
}


}
