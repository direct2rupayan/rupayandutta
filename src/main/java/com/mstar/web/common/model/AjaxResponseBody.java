package com.mstar.web.common.model;
import java.util.List;

import com.mstar.web.content.entity.Content;
public class AjaxResponseBody {
	String msg;
	List<Content> result;
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public List<Content> getResult() {
		return result;
	}
	public void setResult(List<Content> result) {
		this.result = result;
	}
	

}
