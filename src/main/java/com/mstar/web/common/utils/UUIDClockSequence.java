package com.mstar.web.common.utils;

import java.util.UUID;

public class UUIDClockSequence {
	
	public static String generateUUID() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
}
