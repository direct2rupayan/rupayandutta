package com.mstar.web.common.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import com.mstar.web.account.entity.Events;
import com.mstar.web.account.model.EventsDTO;

public class EventCopyUtility {
	
	private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	public static EventsDTO getEventDTO(Events srcObj) {
		String st=dateFormat.format(srcObj.getEventStart());
		String end=dateFormat.format(srcObj.getEventEnd());
		return new EventsDTO(srcObj.getEventId().toString(),srcObj.getEventName(), st, end, srcObj.getEventDescription(), srcObj.getReceivers(),srcObj.getEventGrade(),srcObj.getEventSubjct(),srcObj.getEventChapter(),srcObj.getEventRemark());
	}
	
	/*public static Events getEvents(EventsDTO srcObj) {
		Date st=dateFormat.parse(srcObj.getStart());
		Date end=dateFormat.parse(srcObj.getEnd());
		return new Events(srcObj.getEventName(),st,end,srcObj.getEventDescription(), srcObj.getReceivers());
	}*/

}
