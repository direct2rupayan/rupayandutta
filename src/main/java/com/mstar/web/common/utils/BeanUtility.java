package com.mstar.web.common.utils;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.beans.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
public class BeanUtility {
/**
 * 
 * @param src
 * @param trg
 * @param props
 */
	public static void copyIterableProperties(Object src, Object trg, Iterable<String> props) {
	    BeanWrapper srcWrap = PropertyAccessorFactory.forBeanPropertyAccess(src);
	    BeanWrapper trgWrap = PropertyAccessorFactory.forBeanPropertyAccess(trg);
	    props.forEach(p -> trgWrap.setPropertyValue(p, srcWrap.getPropertyValue(p)));
	}
	
	/**
	 * @param source
	 * @param target
	 * @param ignoreProperties
	 */
	public static void copyProperties(Object source,Object target,String... ignoreProperties) {
		BeanUtils.copyProperties(source, target, ignoreProperties);
	}
	   
}
