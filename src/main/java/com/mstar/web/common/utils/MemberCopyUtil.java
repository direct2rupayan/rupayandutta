package com.mstar.web.common.utils;

import com.mstar.web.account.entity.Member;
import com.mstar.web.account.entity.MemberGroup;
import com.mstar.web.account.model.MemberDTO;
import com.mstar.web.utils.EncrytedPasswordUtils;

public class MemberCopyUtil {
	
	public static Member getmemberEntity(MemberDTO memberDTO){
		MemberGroup mg= new MemberGroup();
		mg.setGroupID(memberDTO.getMemberGroupID());
		Member member=new Member();
		member.setMemberGroup(mg);
		member.setLogin(memberDTO.getLogin());
		member.setEncrytedPassword(EncrytedPasswordUtils.encrytePassword(memberDTO.getEncrytedPassword()));
		member.setActive(memberDTO.getActive());
		mg.setMember(member);
		return member;
	}

}
