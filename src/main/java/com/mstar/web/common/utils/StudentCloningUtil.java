package com.mstar.web.common.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
//import org.assertj.core.util.Arrays;

import com.mstar.web.account.entity.Parent;
import com.mstar.web.account.entity.Student;
import com.mstar.web.account.model.StudentDTO;
public class StudentCloningUtil {
	
	public static void fromDtoToEntity(StudentDTO srcDtoObj,Student destEntityObj) {
		String[] exclude={"pId","pName","pRelation","pOccupation","pEmail","pPhone"};
		String[] include={"pId","pName","pRelation","pOccupation","pEmail","pPhone"};
		BeanUtility.copyProperties(srcDtoObj, destEntityObj, exclude);
		setNamedPropertiesForStudentEntity(srcDtoObj, destEntityObj, include);
	}
	
	public static void fromDtoToEntityforUpdateAndView(StudentDTO srcDtoObj,Student destEntityObj) {
		String[] exclude={"pId"};
		String[] include={"pName","pRelation","pOccupation","pEmail","pPhone"};
		BeanUtility.copyProperties(srcDtoObj, destEntityObj, exclude);
		setNamedPropertiesForStudentEntity(srcDtoObj, destEntityObj, include);
	}
	

    public static void fromEntityToDto(Student srcEntityObj,StudentDTO destDtoObj) {
    	String[] exclude={"parent"};
    	BeanUtility.copyProperties(srcEntityObj, destDtoObj, exclude);
    	setNamedPropertiesForStudentDTO(srcEntityObj,destDtoObj);
	}
    
	private static void setNamedPropertiesForStudentEntity(StudentDTO source,Student target,String... includeProperties) {
		
		List<Object> props=Arrays.asList(includeProperties);
		
		try {
			if(props.contains("pId"))
			PropertyUtils.setNestedProperty(target, "parent.pId", source.getpId());
			
			PropertyUtils.setNestedProperty(target, "parent.pName", source.getpName());
			PropertyUtils.setNestedProperty(target, "parent.pRelation", source.getpRelation());
			PropertyUtils.setNestedProperty(target, "parent.pOccupation", source.getpOccupation());
			PropertyUtils.setNestedProperty(target, "parent.pEmail", source.getpEmail());
			PropertyUtils.setNestedProperty(target, "parent.pPhone", source.getpPhone());
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void setNamedPropertiesForStudentDTO(Student source,StudentDTO target) {
		Parent p = source.getParent();
		try {
			PropertyUtils.setNestedProperty(target, "pId", p.getpId());
			PropertyUtils.setNestedProperty(target, "pName", p.getpName());
			PropertyUtils.setNestedProperty(target, "pRelation", p.getpRelation());
			PropertyUtils.setNestedProperty(target, "pOccupation", p.getpOccupation());
			PropertyUtils.setNestedProperty(target, "pEmail", p.getpEmail());
			PropertyUtils.setNestedProperty(target, "pPhone", p.getpPhone());
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
